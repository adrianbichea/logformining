﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogForMining.Utils
{
    public static class Language
    {
        static int CurrentLanguage { get; set; }
        static Dictionary<string, string> LanguageDictionary { get; set; }
        static DB.Language.AllLanguages al { get; set; }
        public static void LoadLanguageDefault()
        {
            al = DB.Language.AllLanguages.Load();
            CurrentLanguage = al.CurrentLanguage;
            LanguageDictionary = new Dictionary<string, string>();
            var ll = DB.Language.LanguageLang.ReadFromXML(al.Languages[CurrentLanguage].LangFile);
            foreach (var txt in ll.LanguageText)
            {
                if (!LanguageDictionary.ContainsKey(txt.LangKey))
                {
                    LanguageDictionary.Add(txt.LangKey, txt.LangString);
                }
            }
        }

        public static string GetText(string key)
        {
            if (LanguageDictionary.ContainsKey(key))
            {
                return LanguageDictionary[key];
            }
            return key;
        }

        public static void ChangeLanguage(string lang)
        {
            var idx = al.Languages.FindIndex(x => x.LangName == lang);
            if (CurrentLanguage != idx)
            {
                al.CurrentLanguage = idx;
                al.Save();
                LoadLanguageDefault();
            }
        }

        //public static void ShouldNotBeUsed_HAHAHA()
        //{
        //    al = new DB.Language.AllLanguages();
        //    al.CurrentLanguage = 0;
        //    al.Languages.Add(new DB.Language.Language() { LangFile = "lang_en.xml", LangName = "en" });
        //    al.Save();
        //    var l = new DB.Language.LanguageLang();
        //    l.LanguageText.Add(new DB.Language.LanguageText() { LangKey = "{TotalDecay}", LangString = "Total Decay" });
        //    l.SaveToXML("lang_en.xml");
        //}
    }
}
