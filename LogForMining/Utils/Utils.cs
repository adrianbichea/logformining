﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining.Utils
{
    public static class Utils
    {
        public static int itemPerPage = 16;

        public static void SetDoubleBuffered(DataGridView tbl)
        {
            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.SetProperty, null,
                        tbl, new object[] { true });
        }
    }
}
