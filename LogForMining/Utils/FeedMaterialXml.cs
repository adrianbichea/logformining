﻿using LogForMining.DB.Material;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogForMining.Utils
{
    //this class is used only to feed the material xml with infos
    //will be used 1 time..
    //i dont check errors because is used in debug mode :S
    public static class FeedMaterialXml
    {
        static Dictionary<int, string> matType = new Dictionary<int, string>();

        public static void Feed(string filename)
        {
            matType.Add(0, "Ore");
            matType.Add(1, "Enmatter");
            matType.Add(2, "Treasure");

            if (File.Exists("materials.xml"))
            {
                File.Delete("materials.xml");
            }
            AllMaterials am = new AllMaterials();
            var lines = File.ReadAllLines(filename);//this is a copy and paste file from entropiawiki to a notepad text file
            am.Materials.Add( new DB.Material.Material() { Id = ++am.LastId, CurrentMU = 100, Name = "TT Material", TTValue = 0.01, MaterialType = 0 });
            foreach (var s in lines)
            {
                var a = s.Split('\t');
                double ttval = double.Parse(a[4]);
                int t = matType.FirstOrDefault(x => x.Value == a[3]).Key;
                Material m = new DB.Material.Material() { Id = ++am.LastId, CurrentMU = 100, Name = a[2], TTValue = ttval, MaterialType = t };
                am.Materials.Add(m);
            }
            am.Save();
        }
    }
}
