﻿using LogForMining.DB.Decay;
using LogForMining.DB.Income;
using LogForMining.DB.Material;
using LogForMining.DB.Result;
using LogForMining.Forms.MaterialForm;
using LogForMining.Forms.Result;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining
{
    public partial class Main : Form
    {
        string txtTotal { get; set; }
        string txtTotalIncome { get; set; }
        string txtResult { get; set; }
        string txtTotalIncomeMU { get; set; }
        string txtResultMU { get; set; }
        string txtTotalMUOnMats { get; set; }

        public Main()
        {
            InitializeComponent();
        }

        private void materialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaterialForm mf = new Forms.MaterialForm.MaterialForm();
            mf.ShowDialog();
        }

        private void BtnResults_Click(object sender, EventArgs e)
        {
            ResultForm rf = new Forms.Result.ResultForm();
            rf.ShowDialog();
            ShowTotal();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            FeedLanguagesInMenu();
            //Utils.FeedMaterialXml.Feed("mats.txt");
            //Utils.Language.ShouldNotBeUsed_HAHAHA();
            Utils.Language.LoadLanguageDefault();
            ShowText();
            ShowTotal();
        }

        private void ShowText()
        {
            txtTotal = Utils.Language.GetText("{TotalDecay}");
            txtTotalIncome = Utils.Language.GetText("{TotalIncome}");
            txtResult = Utils.Language.GetText("{ResultTT}");
            txtTotalIncomeMU = Utils.Language.GetText("{TotalIncomeWithMU}");
            txtResultMU = Utils.Language.GetText("{TotalResultWithMU}");
            txtTotalMUOnMats = Utils.Language.GetText("{TotalMUOnMaterials}");
            BtnResults.Text = Utils.Language.GetText("{BtnResult_MainForm}");
            GrpBox.Text = Utils.Language.GetText("{MainGrpBox}");
            Text = Utils.Language.GetText("{MainForm}");
            OptionsMenu.Text = Utils.Language.GetText("{OptionsMenu}");
            Options_Material.Text = Utils.Language.GetText("{MaterialsMenu}");
            Options_Language.Text = Utils.Language.GetText("{LanguageMenu}");
        }

        private void FeedLanguagesInMenu()
        {
            DB.Language.AllLanguages al = DB.Language.AllLanguages.Load();
            foreach(var l in al.Languages)
            {
                Options_Language.DropDownItems.Add(l.LangName, null, LanguageClick);
            }
        }

        private void LanguageClick(object sender, EventArgs e)
        {
            var clickedItem = (ToolStripMenuItem)sender;
            Utils.Language.ChangeLanguage(clickedItem.Text);
            ShowText();
            ShowTotal();
        }

        private void ShowTotal()
        {
            AllDecays ad = AllDecays.Load();
            AllIncomes ai = AllIncomes.Load();
            AllMaterials am = AllMaterials.Load();

            double decay = 0;
            foreach(var d in ad.Decays)
            {
                decay += d.Value;
            }

            double income = 0;
            double incomeMU = 0;
            foreach (var i in ai.Incomes)
            {
                double val = 0;
                if (i.Quantity == -1)
                {
                    val = i.Value;
                }
                else
                {
                    var m = am.Materials.Find(x => x.Id == i.IdMaterial);
                    val = m.TTValue * i.Quantity;
                }
                income += val;
                incomeMU += val * i.MU * 0.01;
            }

            LblTotalDecay.Text = txtTotal + ": " + decay.ToString();
            LblTotalIncome.Text = txtTotalIncome + ": " + income.ToString();
            LblResult.Text = $"{txtResult}: {(income - decay).ToString()} ({(income * 100 / decay).ToString("0.000")}%)";
            LblTotalIncomeMU.Text = txtTotalIncomeMU + ": " + incomeMU.ToString("0.00");
            LblResultMU.Text = txtResultMU + ": " + (incomeMU - decay).ToString("0.00");
            LblTotalMUOnMats.Text = txtTotalMUOnMats + ": " + (incomeMU * 100 / income).ToString("0.00");
        }
    }
}
