﻿namespace LogForMining.Forms.Result
{
    partial class ResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultForm));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.PanelDown = new System.Windows.Forms.Panel();
            this.PgUp = new System.Windows.Forms.Button();
            this.PgDown = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.Table = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDecay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIncome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWithMU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResultWithMU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRoundMU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelUp = new System.Windows.Forms.Panel();
            this.BtnRemove = new System.Windows.Forms.Button();
            this.BtnModif = new System.Windows.Forms.Button();
            this.BtnNew = new System.Windows.Forms.Button();
            this.PanelDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).BeginInit();
            this.PanelUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "lorry-icon.png");
            this.imageList1.Images.SetKeyName(1, "Edit-Document-icon.png");
            this.imageList1.Images.SetKeyName(2, "remove-from-database-icon.png");
            // 
            // PanelDown
            // 
            this.PanelDown.Controls.Add(this.PgUp);
            this.PanelDown.Controls.Add(this.PgDown);
            this.PanelDown.Controls.Add(this.lblPage);
            this.PanelDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelDown.Location = new System.Drawing.Point(0, 464);
            this.PanelDown.Name = "PanelDown";
            this.PanelDown.Size = new System.Drawing.Size(779, 41);
            this.PanelDown.TabIndex = 6;
            // 
            // PgUp
            // 
            this.PgUp.Location = new System.Drawing.Point(285, 9);
            this.PgUp.Name = "PgUp";
            this.PgUp.Size = new System.Drawing.Size(52, 23);
            this.PgUp.TabIndex = 11;
            this.PgUp.Text = "<<";
            this.PgUp.UseVisualStyleBackColor = true;
            this.PgUp.Click += new System.EventHandler(this.PgUp_Click);
            // 
            // PgDown
            // 
            this.PgDown.Location = new System.Drawing.Point(441, 9);
            this.PgDown.Name = "PgDown";
            this.PgDown.Size = new System.Drawing.Size(52, 23);
            this.PgDown.TabIndex = 10;
            this.PgDown.Text = ">>";
            this.PgDown.UseVisualStyleBackColor = true;
            this.PgDown.Click += new System.EventHandler(this.PgDown_Click);
            // 
            // lblPage
            // 
            this.lblPage.Location = new System.Drawing.Point(343, 9);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(92, 23);
            this.lblPage.TabIndex = 9;
            this.lblPage.Text = "label1";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Table
            // 
            this.Table.AllowUserToAddRows = false;
            this.Table.AllowUserToDeleteRows = false;
            this.Table.AllowUserToResizeColumns = false;
            this.Table.AllowUserToResizeRows = false;
            this.Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colDecay,
            this.colIncome,
            this.colResult,
            this.colPercent,
            this.colWithMU,
            this.colResultWithMU,
            this.colRoundMU});
            this.Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Table.Location = new System.Drawing.Point(0, 78);
            this.Table.MultiSelect = false;
            this.Table.Name = "Table";
            this.Table.ReadOnly = true;
            this.Table.RowHeadersVisible = false;
            this.Table.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Table.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Table.Size = new System.Drawing.Size(779, 427);
            this.Table.TabIndex = 5;
            this.Table.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table_CellDoubleClick);
            // 
            // colId
            // 
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Width = 40;
            // 
            // colDecay
            // 
            this.colDecay.HeaderText = "Decay";
            this.colDecay.Name = "colDecay";
            this.colDecay.ReadOnly = true;
            // 
            // colIncome
            // 
            this.colIncome.HeaderText = "Income";
            this.colIncome.Name = "colIncome";
            this.colIncome.ReadOnly = true;
            // 
            // colResult
            // 
            this.colResult.HeaderText = "Result";
            this.colResult.Name = "colResult";
            this.colResult.ReadOnly = true;
            // 
            // colPercent
            // 
            this.colPercent.HeaderText = "%";
            this.colPercent.Name = "colPercent";
            this.colPercent.ReadOnly = true;
            // 
            // colWithMU
            // 
            this.colWithMU.HeaderText = "With MU";
            this.colWithMU.Name = "colWithMU";
            this.colWithMU.ReadOnly = true;
            // 
            // colResultWithMU
            // 
            this.colResultWithMU.HeaderText = "Result MU";
            this.colResultWithMU.Name = "colResultWithMU";
            this.colResultWithMU.ReadOnly = true;
            // 
            // colRoundMU
            // 
            this.colRoundMU.HeaderText = "Round MU";
            this.colRoundMU.Name = "colRoundMU";
            this.colRoundMU.ReadOnly = true;
            // 
            // PanelUp
            // 
            this.PanelUp.Controls.Add(this.BtnRemove);
            this.PanelUp.Controls.Add(this.BtnModif);
            this.PanelUp.Controls.Add(this.BtnNew);
            this.PanelUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelUp.Location = new System.Drawing.Point(0, 0);
            this.PanelUp.Name = "PanelUp";
            this.PanelUp.Size = new System.Drawing.Size(779, 78);
            this.PanelUp.TabIndex = 7;
            // 
            // BtnRemove
            // 
            this.BtnRemove.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRemove.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnRemove.ImageIndex = 2;
            this.BtnRemove.ImageList = this.imageList1;
            this.BtnRemove.Location = new System.Drawing.Point(365, 12);
            this.BtnRemove.Name = "BtnRemove";
            this.BtnRemove.Size = new System.Drawing.Size(175, 60);
            this.BtnRemove.TabIndex = 5;
            this.BtnRemove.Text = "Remove";
            this.BtnRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnRemove.UseVisualStyleBackColor = true;
            this.BtnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
            // 
            // BtnModif
            // 
            this.BtnModif.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnModif.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnModif.ImageIndex = 1;
            this.BtnModif.ImageList = this.imageList1;
            this.BtnModif.Location = new System.Drawing.Point(184, 12);
            this.BtnModif.Name = "BtnModif";
            this.BtnModif.Size = new System.Drawing.Size(175, 60);
            this.BtnModif.TabIndex = 4;
            this.BtnModif.Text = "Modify";
            this.BtnModif.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnModif.UseVisualStyleBackColor = true;
            this.BtnModif.Click += new System.EventHandler(this.BtnModif_Click);
            // 
            // BtnNew
            // 
            this.BtnNew.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNew.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnNew.ImageIndex = 0;
            this.BtnNew.ImageList = this.imageList1;
            this.BtnNew.Location = new System.Drawing.Point(3, 12);
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.Size = new System.Drawing.Size(175, 60);
            this.BtnNew.TabIndex = 3;
            this.BtnNew.Text = "New";
            this.BtnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnNew.UseVisualStyleBackColor = true;
            this.BtnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // ResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 505);
            this.Controls.Add(this.PanelDown);
            this.Controls.Add(this.Table);
            this.Controls.Add(this.PanelUp);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ResultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ResultForm";
            this.Load += new System.EventHandler(this.ResultForm_Load);
            this.PanelDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table)).EndInit();
            this.PanelUp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel PanelDown;
        private System.Windows.Forms.DataGridView Table;
        private System.Windows.Forms.Panel PanelUp;
        private System.Windows.Forms.Button BtnRemove;
        private System.Windows.Forms.Button BtnModif;
        private System.Windows.Forms.Button BtnNew;
        private System.Windows.Forms.Button PgUp;
        private System.Windows.Forms.Button PgDown;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDecay;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIncome;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWithMU;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResultWithMU;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRoundMU;
    }
}