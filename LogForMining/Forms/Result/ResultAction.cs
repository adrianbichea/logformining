﻿using LogForMining.DB.Decay;
using LogForMining.Forms.Decay;
using LogForMining.Forms.Income;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining.Forms.Result
{
    public partial class ResultAction : Form
    {
        long id { get; set; }
        public object AllIncome { get; private set; }

        public ResultAction(long idRound)
        {
            InitializeComponent();
            id = idRound;
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            DecayForm df = new Decay.DecayForm(id);
            df.ShowDialog();
            ShowTotals();
        }

        private void BtnIncome_Click(object sender, EventArgs e)
        {
            IncomeForm ifrm = new Income.IncomeForm(id);
            ifrm.ShowDialog();
            ShowTotals();
        }

        private void ResultAction_Load(object sender, EventArgs e)
        {
            ShowLang();
            ShowTotals();
        }

        private void ShowLang()
        {
            Text = Utils.Language.GetText("{ResultActionForm}");
            BtnDecay.Text = Utils.Language.GetText("{Decay}");
            BtnIncome.Text = Utils.Language.GetText("{Income}");
            LblMU.Text = Utils.Language.GetText("{WithMU}");
            BtnOk.Text = Utils.Language.GetText("{Ok}");
        }

        private void ShowTotals()
        {
            AllDecays ad = AllDecays.Load();
            var dList = ad.Decays.FindAll(x => x.IdRound == id);
            double totalDecay = 0.0;
            foreach(var d in dList)
            {
                totalDecay += d.Value;
            }

            var am = DB.Material.AllMaterials.Load();
            var ai = DB.Income.AllIncomes.Load();
            var iList = ai.Incomes.FindAll(x => x.IdRound == id);
            double totalIncome = 0.0;
            double totalIncomeMU = 0.0;
            foreach(var i in iList)
            {
                if (i.Quantity == -1)
                {
                    totalIncome += i.Value;
                    totalIncomeMU += i.Value * i.MU * 0.01;
                }
                else if (i.Value == -1)
                {
                    var m = am.Materials.Find(x => x.Id == i.IdMaterial);
                    double val = i.Quantity * m.TTValue;
                    totalIncome += val;
                    totalIncomeMU += val * i.MU * 0.01;
                }
            }

            LblDecay.Text = totalDecay.ToString();
            LblIncome.Text = totalIncome.ToString();
            LblIncomeMU.Text = totalIncomeMU.ToString("0.00");

            double result = totalIncome - totalDecay;
            double resultMU = totalIncomeMU - totalDecay;

            GrpBox.Text = $"{Utils.Language.GetText("{ResultAction_Grp}")} -> ";
            GrpBox.Text += $"({Utils.Language.GetText("{TT}")}: {result.ToString("0.00")})";
            GrpBox.Text += $"({Utils.Language.GetText("{MU}")}: {resultMU.ToString("0.00")})" ;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
