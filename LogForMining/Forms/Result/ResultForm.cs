﻿using LogForMining.DB.Decay;
using LogForMining.DB.Income;
using LogForMining.DB.Material;
using LogForMining.DB.Result;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining.Forms.Result
{
    public partial class ResultForm : Form
    {
        int maxPages { get; set; }
        int currentPage { get; set; }
        int lastIndexClicked { get; set; }
        AllIncomes ai { get; set; }
        AllDecays ad { get; set; }
        AllMaterials am { get; set; }

        public ResultForm()
        {
            InitializeComponent();
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {
            am = AllMaterials.Load();
            Utils.Utils.SetDoubleBuffered(Table);
            currentPage = 1;
            ShowTable();
            foreach (DataGridViewColumn column in Table.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            ChangeLang();
        }

        private void ButtonPages()
        {
            PgDown.Enabled = true;
            PgUp.Enabled = true;
            if (currentPage == 1)
            {
                PgUp.Enabled = false;
            }
            if (currentPage == maxPages || maxPages == 0)
            {
                PgDown.Enabled = false;
            }
        }

        private void ChangeLang()
        {
            Text = Utils.Language.GetText("{ResultForm}");
            BtnNew.Text = Utils.Language.GetText("{BtnNew}");
            BtnModif.Text = Utils.Language.GetText("{BtnModify}");
            BtnRemove.Text = Utils.Language.GetText("{BtnDelete}");
            colId.HeaderText = Utils.Language.GetText("{Id}");
            colDecay.HeaderText = Utils.Language.GetText("{Decay}");
            colIncome.HeaderText = Utils.Language.GetText("{Income}");
            colPercent.HeaderText = Utils.Language.GetText("{Percent}");
            colWithMU.HeaderText = Utils.Language.GetText("{WithMU}");
            colResultWithMU.HeaderText = Utils.Language.GetText("{ResultWithMU}");
            colRoundMU.HeaderText = Utils.Language.GetText("{RoundMU}");
        }

        private void ShowTable()
        {
            AllResults ar = AllResults.Load();
            if (ar == null)
            {
                ButtonPages();
                return;
            }
            ai = AllIncomes.Load();
            ad = AllDecays.Load();
            Table.Rows.Clear();
            var list = ar.Results.OrderByDescending(x => x.Id).ToList();

            var itemPerPage = Utils.Utils.itemPerPage;
            maxPages = (int)Math.Ceiling((double)list.Count / (double)itemPerPage);
            if (currentPage > maxPages && maxPages > 0)
            {
                currentPage = maxPages;
            }
            var lastPageCount = list.Count % itemPerPage;
            if (list.Count > 0 && lastPageCount == 0)
            {
                lastPageCount = itemPerPage;
            }
            list = list.GetRange((currentPage - 1) * itemPerPage, currentPage < maxPages ? itemPerPage : lastPageCount);
            lblPage.Text = currentPage.ToString() + " / " + maxPages.ToString();
            foreach (var a in list)
            {
                Table.Rows.Add();
                var row = (DataGridViewRow)Table.Rows[Table.Rows.Count - 1];
                row.Cells[colId.Index].Value = a.Id.ToString();

                var decay = GetDecay(a.Id);
                row.Cells[colDecay.Index].Value = decay.ToString("0.00");

                var strIncome = GetIncome(a.Id);
                var arrIncome = strIncome.Split('|');
                row.Cells[colIncome.Index].Value = arrIncome[0];
                row.Cells[colWithMU.Index].Value = arrIncome[1];

                double income = double.Parse(arrIncome[0]);
                double incomeMU = double.Parse(arrIncome[1]);

                row.Cells[colResult.Index].Value = (income - decay).ToString("0.00");
                row.Cells[colResultWithMU.Index].Value = (incomeMU - decay).ToString("0.00");

                row.Cells[colRoundMU.Index].Value = (incomeMU * 100 / income).ToString("0.000");

                row.Cells[colPercent.Index].Value = (income * 100 / decay).ToString("0.000");
            }
            if (list.Count > 0)
            {
                Table.Rows[lastIndexClicked].Selected = true;
            }
            ButtonPages();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You're about to create a new result?\nIt will be written into the DB (xml ;) )", "Do you?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AllResults ar = AllResults.Load();
                ar.Results.Add(new DB.Result.Result() { Id = ++ar.LastId, Date = DateTime.Now });
                ar.Save();
                ResultAction ra = new Result.ResultAction(ar.LastId);
                ra.ShowDialog();
                ShowTable();
            }
        }

        private double GetDecay(long id)
        {
            var d = ad.Decays.FindAll(x => x.IdRound == id);
            double decay = 0;
            foreach (var it in d)
            {
                decay += it.Value;
            }
            return decay;
        }

        private string GetIncome(long id)
        {
            var inc = ai.Incomes.FindAll(x => x.IdRound == id);
            double income = 0;
            double incomeMU = 0;
            foreach (var i in inc)
            {
                double val = 0;
                if (i.Quantity == -1)
                {
                    val = i.Value;
                }
                else if (i.Value == -1)
                {
                    var m = am.Materials.Find(x => x.Id == i.IdMaterial);
                    val = i.Quantity * m.TTValue;
                }
                income += val;
                incomeMU += val * i.MU * 0.01;
            }
            return income.ToString("0.00") + "|" + incomeMU.ToString("0.00");
        }

        private void BtnModif_Click(object sender, EventArgs e)
        {
            if (Table.SelectedRows.Count < 1)
            {
                return;
            }
            lastIndexClicked = Table.SelectedRows[0].Index;
            var id = long.Parse(Table.SelectedRows[0].Cells[colId.Index].Value.ToString());
            ResultAction ra = new Result.ResultAction(id);
            ra.ShowDialog();
            ShowTable();
        }

        private void Table_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnModif_Click(null, null);
        }

        private void PgDown_Click(object sender, EventArgs e)
        {
            if (currentPage < maxPages)
            {
                currentPage++;
                ShowTable();
            }
        }

        private void PgUp_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                ShowTable();
            }
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            if (Table.SelectedRows.Count < 1)
            {
                return;
            }
            int index = int.Parse(Table.SelectedRows[0].Cells[colId.Index].Value.ToString());
            if (MessageBox.Show($"Do you want to delete this field? (Id:{index})", "Really?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AllResults ar = AllResults.Load();
                var m = ar.Results.Find(x => x.Id == index);
                if (m != null)
                {
                    ar.Results.Remove(m);
                    ar.Save();
                    ShowTable();
                }
                else
                {
                    MessageBox.Show("Something went wrong!");
                }
            }
        }
    }
}