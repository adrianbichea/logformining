﻿namespace LogForMining.Forms.Result
{
    partial class ResultAction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultAction));
            this.GrpBox = new System.Windows.Forms.GroupBox();
            this.LblMU = new System.Windows.Forms.Label();
            this.LblIncomeMU = new System.Windows.Forms.Label();
            this.LblIncome = new System.Windows.Forms.Label();
            this.LblDecay = new System.Windows.Forms.Label();
            this.BtnIncome = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.BtnDecay = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.GrpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpBox
            // 
            this.GrpBox.Controls.Add(this.LblMU);
            this.GrpBox.Controls.Add(this.LblIncomeMU);
            this.GrpBox.Controls.Add(this.LblIncome);
            this.GrpBox.Controls.Add(this.LblDecay);
            this.GrpBox.Controls.Add(this.BtnIncome);
            this.GrpBox.Controls.Add(this.BtnDecay);
            this.GrpBox.Location = new System.Drawing.Point(12, 12);
            this.GrpBox.Name = "GrpBox";
            this.GrpBox.Size = new System.Drawing.Size(376, 233);
            this.GrpBox.TabIndex = 0;
            this.GrpBox.TabStop = false;
            this.GrpBox.Text = "Result";
            // 
            // LblMU
            // 
            this.LblMU.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMU.Location = new System.Drawing.Point(6, 162);
            this.LblMU.Name = "LblMU";
            this.LblMU.Size = new System.Drawing.Size(171, 60);
            this.LblMU.TabIndex = 9;
            this.LblMU.Text = "With MU";
            this.LblMU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblIncomeMU
            // 
            this.LblIncomeMU.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblIncomeMU.Location = new System.Drawing.Point(183, 159);
            this.LblIncomeMU.Name = "LblIncomeMU";
            this.LblIncomeMU.Size = new System.Drawing.Size(187, 60);
            this.LblIncomeMU.TabIndex = 8;
            this.LblIncomeMU.Text = "0.00";
            this.LblIncomeMU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblIncome
            // 
            this.LblIncome.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblIncome.Location = new System.Drawing.Point(183, 99);
            this.LblIncome.Name = "LblIncome";
            this.LblIncome.Size = new System.Drawing.Size(187, 60);
            this.LblIncome.TabIndex = 7;
            this.LblIncome.Text = "0.00";
            this.LblIncome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblDecay
            // 
            this.LblDecay.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDecay.Location = new System.Drawing.Point(183, 33);
            this.LblDecay.Name = "LblDecay";
            this.LblDecay.Size = new System.Drawing.Size(187, 60);
            this.LblDecay.TabIndex = 6;
            this.LblDecay.Text = "0.00";
            this.LblDecay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnIncome
            // 
            this.BtnIncome.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnIncome.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnIncome.ImageIndex = 1;
            this.BtnIncome.ImageList = this.imageList1;
            this.BtnIncome.Location = new System.Drawing.Point(6, 99);
            this.BtnIncome.Name = "BtnIncome";
            this.BtnIncome.Size = new System.Drawing.Size(171, 60);
            this.BtnIncome.TabIndex = 5;
            this.BtnIncome.Text = "Income";
            this.BtnIncome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnIncome.UseVisualStyleBackColor = true;
            this.BtnIncome.Click += new System.EventHandler(this.BtnIncome_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "price-tag-icon.png");
            this.imageList1.Images.SetKeyName(1, "Money-icon.png");
            // 
            // BtnDecay
            // 
            this.BtnDecay.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDecay.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnDecay.ImageIndex = 0;
            this.BtnDecay.ImageList = this.imageList1;
            this.BtnDecay.Location = new System.Drawing.Point(6, 33);
            this.BtnDecay.Name = "BtnDecay";
            this.BtnDecay.Size = new System.Drawing.Size(171, 60);
            this.BtnDecay.TabIndex = 4;
            this.BtnDecay.Text = "Decay";
            this.BtnDecay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnDecay.UseVisualStyleBackColor = true;
            this.BtnDecay.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(274, 251);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(108, 23);
            this.BtnOk.TabIndex = 7;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // ResultAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 286);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.GrpBox);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ResultAction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ResultAction";
            this.Load += new System.EventHandler(this.ResultAction_Load);
            this.GrpBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBox;
        private System.Windows.Forms.Button BtnDecay;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button BtnIncome;
        private System.Windows.Forms.Label LblIncome;
        private System.Windows.Forms.Label LblDecay;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Label LblMU;
        private System.Windows.Forms.Label LblIncomeMU;
    }
}