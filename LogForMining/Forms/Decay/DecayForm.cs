﻿using LogForEu.DB.Decay;
using LogForMining.DB.Decay;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LogForMining.Forms.Decay
{
    public partial class DecayForm : Form
    {
        long idRound { get; set; }
        long idModif { get; set; }
        AllDecays ad { get; set; }

        List<ComboBox> lbls { get; set; }
        List<TextBox> txtValues { get; set; }
        List<Label> lPED { get; set; }

        DecayType dt { get; set; }

        public DecayForm(long idRound)
        {
            InitializeComponent();
            this.idRound = idRound;
            idModif = -1;
        }

        private void DecayForm_Load(object sender, EventArgs e)
        {
            lbls = new List<ComboBox>();
            dt = DecayType.Load();
            lPED = new List<Label>();
            txtValues = new List<TextBox>();
            ShowLang();//just to update on git
            ad = AllDecays.Load();
            var d = ad.Decays.FindAll(x => x.IdRound == idRound);
            if (d.Count > 0)
            {
                //TxtProbes.Text = d.TTProbes.ToString();
                idModif = d[0].Id;
                foreach (var a in d)
                {
                    if (dt.DecaysType.Count < a.DecayType)
                    {
                        AddControl(-1, a.Value);
                    }
                    else
                    {
                        AddControl(a.DecayType, a.Value);
                    }
                }
            }
            //AddControl();
        }

        private void ShowLang()
        {
            Text = Utils.Language.GetText("{MaterialFormText}");
            GrpBox.Text = Utils.Language.GetText("{InfoMaterial}");
            BtnOk.Text = Utils.Language.GetText("{Ok}");
            BtnCancel.Text = Utils.Language.GetText("{Cancel}");
            /*LblProbes.Text = Utils.Language.GetText("{Probes}");*/
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            var d = GetDecayFromForm(idRound);
            if (d.Count == 0)
            {
                return;
            }
            if (idModif != -1)//is a modify operation
            {
                ad.Decays.RemoveAll(x => x.IdRound == idRound);
                foreach (var a in d)
                {
                    a.Id = ++ad.LastId;
                    ad.Decays.Add(a);
                }
            }
            else
            {
                foreach (var a in d)
                {
                    a.Id = ++ad.LastId;
                    ad.Decays.Add(a);
                }
            }
            ad.Save();
            this.Close();
        }

        private List<DB.Decay.Decay> GetDecayFromForm(long idRound)
        {
            double fDecay = 0;

            List<DB.Decay.Decay> decay = new List<DB.Decay.Decay>();
            for (int i = 0; i < lbls.Count; i++)
            {
                DB.Decay.Decay d = new DB.Decay.Decay();
                if (lbls[i].SelectedIndex == -1)
                {
                    return new List<DB.Decay.Decay>();
                }
                d.DecayType = lbls[i].SelectedIndex;
                if (!double.TryParse(txtValues[i].Text, out fDecay))
                {
                    return new List<DB.Decay.Decay>();
                }
                d.Value = fDecay;
                d.IdRound = idRound;
                d.Id = -1;
                decay.Add(d);
            }

            return decay;
        }

        private DB.Decay.Decay NullDecay()
        {
            return new DB.Decay.Decay() { Value = -1 };
        }

        private int GetY(int baseY)
        {
            return baseY + (29 * lbls.Count);
        }

        private void AddControl(int comboIndex = -1, double value = -1)
        {
            ComboBox c = new ComboBox();
            foreach (var t in dt.DecaysType)
            {
                c.Items.Add(t);
            }
            c.Parent = GrpBox;
            c.Left = 7;
            c.Top = GetY(33);
            c.Size = new System.Drawing.Size(168, 23);
            c.DropDownStyle = ComboBoxStyle.DropDownList;
            c.Name = $"Combo{lbls.Count + 1}";
            if (comboIndex != -1)
            {
                c.SelectedIndex = comboIndex;
            }

            TextBox tb = new TextBox();
            tb.Parent = GrpBox;
            tb.Left = 181;
            tb.Top = GetY(33);
            tb.Name = $"Value{txtValues.Count + 1}";
            if (value != -1)
            {
                tb.Text = value.ToString("R");
            }

            Label l = new Label();
            l.Parent = GrpBox;
            l.Left = 296;
            l.Top = GetY(39);
            l.Text = "PED";

            lbls.Add(c);
            txtValues.Add(tb);
            lPED.Add(l);

            this.Height += 29;
            GrpBox.Height += 29;
            BtnOk.Top += 29;
            BtnCancel.Top += 29;

        }

        private void RemoveLastControl()
        {
            this.Height -= 29;
            GrpBox.Height -= 29;
            BtnOk.Top -= 29;
            BtnCancel.Top -= 29;

            GrpBox.Controls.Remove(lbls[lbls.Count - 1]);
            lbls.RemoveAt(lbls.Count - 1);

            GrpBox.Controls.Remove(txtValues[txtValues.Count - 1]);
            txtValues.RemoveAt(txtValues.Count - 1);

            GrpBox.Controls.Remove(lPED[lPED.Count - 1]);
            lPED.RemoveAt(lPED.Count - 1);
        }

        private void AddDecay_Click(object sender, EventArgs e)
        {
            AddControl();
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            RemoveLastControl();
        }
    }
}
