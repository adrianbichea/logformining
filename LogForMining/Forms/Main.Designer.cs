﻿namespace LogForMining
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.OptionsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.Options_Material = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.Options_Language = new System.Windows.Forms.ToolStripMenuItem();
            this.GrpBox = new System.Windows.Forms.GroupBox();
            this.LblTotalMUOnMats = new System.Windows.Forms.Label();
            this.LblResultMU = new System.Windows.Forms.Label();
            this.LblTotalIncomeMU = new System.Windows.Forms.Label();
            this.LblResult = new System.Windows.Forms.Label();
            this.LblTotalIncome = new System.Windows.Forms.Label();
            this.LblTotalDecay = new System.Windows.Forms.Label();
            this.BtnResults = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.Menu.SuspendLayout();
            this.GrpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptionsMenu});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.Menu.Size = new System.Drawing.Size(338, 24);
            this.Menu.TabIndex = 0;
            this.Menu.Text = "menuStrip1";
            // 
            // OptionsMenu
            // 
            this.OptionsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Options_Material,
            this.toolStripMenuItem1,
            this.Options_Language});
            this.OptionsMenu.Name = "OptionsMenu";
            this.OptionsMenu.Size = new System.Drawing.Size(61, 20);
            this.OptionsMenu.Text = "Options";
            // 
            // Options_Material
            // 
            this.Options_Material.Name = "Options_Material";
            this.Options_Material.Size = new System.Drawing.Size(126, 22);
            this.Options_Material.Text = "Materials";
            this.Options_Material.Click += new System.EventHandler(this.materialsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(123, 6);
            // 
            // Options_Language
            // 
            this.Options_Language.Name = "Options_Language";
            this.Options_Language.Size = new System.Drawing.Size(126, 22);
            this.Options_Language.Text = "Language";
            // 
            // GrpBox
            // 
            this.GrpBox.Controls.Add(this.LblTotalMUOnMats);
            this.GrpBox.Controls.Add(this.LblResultMU);
            this.GrpBox.Controls.Add(this.LblTotalIncomeMU);
            this.GrpBox.Controls.Add(this.LblResult);
            this.GrpBox.Controls.Add(this.LblTotalIncome);
            this.GrpBox.Controls.Add(this.LblTotalDecay);
            this.GrpBox.Location = new System.Drawing.Point(12, 27);
            this.GrpBox.Name = "GrpBox";
            this.GrpBox.Size = new System.Drawing.Size(296, 232);
            this.GrpBox.TabIndex = 1;
            this.GrpBox.TabStop = false;
            this.GrpBox.Text = "Stats";
            // 
            // LblTotalMUOnMats
            // 
            this.LblTotalMUOnMats.AutoSize = true;
            this.LblTotalMUOnMats.Location = new System.Drawing.Point(6, 189);
            this.LblTotalMUOnMats.Name = "LblTotalMUOnMats";
            this.LblTotalMUOnMats.Size = new System.Drawing.Size(147, 15);
            this.LblTotalMUOnMats.TabIndex = 5;
            this.LblTotalMUOnMats.Text = "{TotalMUOnMaterials}";
            // 
            // LblResultMU
            // 
            this.LblResultMU.AutoSize = true;
            this.LblResultMU.Location = new System.Drawing.Point(6, 157);
            this.LblResultMU.Name = "LblResultMU";
            this.LblResultMU.Size = new System.Drawing.Size(140, 15);
            this.LblResultMU.TabIndex = 4;
            this.LblResultMU.Text = "{TotalResultWithMU}";
            // 
            // LblTotalIncomeMU
            // 
            this.LblTotalIncomeMU.AutoSize = true;
            this.LblTotalIncomeMU.Location = new System.Drawing.Point(6, 133);
            this.LblTotalIncomeMU.Name = "LblTotalIncomeMU";
            this.LblTotalIncomeMU.Size = new System.Drawing.Size(140, 15);
            this.LblTotalIncomeMU.TabIndex = 3;
            this.LblTotalIncomeMU.Text = "{TotalIncomeWithMU}";
            // 
            // LblResult
            // 
            this.LblResult.AutoSize = true;
            this.LblResult.Location = new System.Drawing.Point(6, 91);
            this.LblResult.Name = "LblResult";
            this.LblResult.Size = new System.Drawing.Size(77, 15);
            this.LblResult.TabIndex = 2;
            this.LblResult.Text = "{ResultTT}";
            // 
            // LblTotalIncome
            // 
            this.LblTotalIncome.AutoSize = true;
            this.LblTotalIncome.Location = new System.Drawing.Point(6, 64);
            this.LblTotalIncome.Name = "LblTotalIncome";
            this.LblTotalIncome.Size = new System.Drawing.Size(98, 15);
            this.LblTotalIncome.TabIndex = 1;
            this.LblTotalIncome.Text = "{TotalIncome}";
            // 
            // LblTotalDecay
            // 
            this.LblTotalDecay.AutoSize = true;
            this.LblTotalDecay.Location = new System.Drawing.Point(6, 39);
            this.LblTotalDecay.Name = "LblTotalDecay";
            this.LblTotalDecay.Size = new System.Drawing.Size(91, 15);
            this.LblTotalDecay.TabIndex = 0;
            this.LblTotalDecay.Text = "{TotalDecay}";
            // 
            // BtnResults
            // 
            this.BtnResults.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnResults.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnResults.ImageIndex = 0;
            this.BtnResults.ImageList = this.imageList1;
            this.BtnResults.Location = new System.Drawing.Point(12, 265);
            this.BtnResults.Name = "BtnResults";
            this.BtnResults.Size = new System.Drawing.Size(153, 61);
            this.BtnResults.TabIndex = 2;
            this.BtnResults.Text = "Results";
            this.BtnResults.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnResults.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnResults.UseVisualStyleBackColor = true;
            this.BtnResults.Click += new System.EventHandler(this.BtnResults_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Search-Results-icon.png");
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 338);
            this.Controls.Add(this.BtnResults);
            this.Controls.Add(this.GrpBox);
            this.Controls.Add(this.Menu);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.Menu;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Log for miners";
            this.Load += new System.EventHandler(this.Main_Load);
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.GrpBox.ResumeLayout(false);
            this.GrpBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem OptionsMenu;
        private System.Windows.Forms.ToolStripMenuItem Options_Material;
        private System.Windows.Forms.GroupBox GrpBox;
        private System.Windows.Forms.Button BtnResults;
        private System.Windows.Forms.Label LblResultMU;
        private System.Windows.Forms.Label LblTotalIncomeMU;
        private System.Windows.Forms.Label LblResult;
        private System.Windows.Forms.Label LblTotalIncome;
        private System.Windows.Forms.Label LblTotalDecay;
        private System.Windows.Forms.Label LblTotalMUOnMats;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem Options_Language;
    }
}

