﻿using LogForMining.DB.Income;
using LogForMining.DB.Material;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining.Forms.Income
{
    public partial class IncomeAction : Form
    {
        DB.Income.Income income { get; set; }
        Dictionary<long, string> combo { get; set; }
        long idRound { get; set; }
        AllMaterials am { get; set; }

        public IncomeAction(long idRound, DB.Income.Income income = null)
        {
            InitializeComponent();
            this.income = income;
            this.idRound = idRound;
        }

        private void FeedComboBox()
        {
            combo = new Dictionary<long, string>();
            am = AllMaterials.Load();
            foreach (var m in am.Materials)
            {
                combo.Add(m.Id, m.Name);
                CmbMaterial.Items.Add(m.Name);
            }
        }

        private void IncomeAction_Load(object sender, EventArgs e)
        {
            ShowLang();
            FeedComboBox();
            if (income != null)
            {
                if (combo.ContainsKey(income.IdMaterial))
                {
                    CmbMaterial.Text = combo[income.IdMaterial];
                    TxtMU.Text = income.MU.ToString();
                    if (income.Quantity != -1)
                    {
                        TxtQuantity.Text = income.Quantity.ToString();
                    }
                    else if (income.Value != -1)
                    {
                        TxtValue.Text = income.Value.ToString();
                    }
                    idRound = income.IdRound;
                }
            }
        }

        private void ShowLang()
        {
            Text = Utils.Language.GetText("{IncomeFormText}");
            GrpBox.Text = Utils.Language.GetText("{InfoMaterial}");
            BtnOk.Text = Utils.Language.GetText("{Ok}");
            BtnCancel.Text = Utils.Language.GetText("{Cancel}");
            LblMat.Text = Utils.Language.GetText("{Material}");
            LblQuant.Text = Utils.Language.GetText("{Quantity}");
            LblValue.Text = Utils.Language.GetText("{TTValue}");
            LblMU.Text = Utils.Language.GetText("{MU}");
        }

        private void TxtQuantity_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TxtQuantity.Text))
            {
                TxtValue.Enabled = false;
            }
            else
            {
                TxtValue.Enabled = true;
            }
        }

        private void TxtValue_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TxtValue.Text))
            {
                TxtQuantity.Enabled = false;
            }
            else
            {
                TxtQuantity.Enabled = true;
            }
        }

        private long GetMaterialId(string matName)
        {
            long id = combo.FirstOrDefault(x => x.Value == matName).Key;
            return id;
        }

        private DB.Income.Income GetIncomeFromForm()
        {
            long quantity;
            double value;
            double mu;

            if (CmbMaterial.SelectedIndex == -1)
            {
                return NullIncome();
            }
            if (string.IsNullOrEmpty(TxtQuantity.Text) && string.IsNullOrEmpty(TxtValue.Text))
            {
                return NullIncome();
            }
            if (!long.TryParse(TxtQuantity.Text, out quantity))
            {
                if (!string.IsNullOrEmpty(TxtQuantity.Text))
                {
                    return NullIncome();
                }
                quantity = -1;
            }
            if (!double.TryParse(TxtValue.Text, out value))
            {
                if (!string.IsNullOrEmpty(TxtValue.Text))
                {
                    return NullIncome();
                }
                value = -1;
            }
            if (!double.TryParse(TxtMU.Text, out mu))
            {
                return NullIncome();
            }

            return new DB.Income.Income()
            {
                Id = income != null ? income.Id : -1,
                IdMaterial = GetMaterialId(CmbMaterial.Text),
                IdRound = idRound,
                Value = value,
                MU = mu,
                Quantity = quantity
            };
        }

        private DB.Income.Income NullIncome()
        {
            return new DB.Income.Income() { IdRound = -1, MU = -1 };
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            var inc = GetIncomeFromForm();
            if (inc.MU != -1)
            {
                Save(inc, income == null);
            }
            else
            {
                MessageBox.Show(Utils.Language.GetText("{VerifyData}"));
            }
        }

        private void Save(DB.Income.Income inc, bool newIncome = true)
        {
            AllIncomes ai = AllIncomes.Load();
            if (newIncome)
            {
                inc.Id = ++ai.LastId;
                ai.Incomes.Add(inc);
            }
            else
            {
                var m = ai.Incomes.Find(x => x.Id == inc.Id);
                if (m != null)
                {
                    m.Id = inc.Id;
                    m.IdMaterial = inc.IdMaterial;
                    m.IdRound = inc.IdRound;
                    m.MU = inc.MU;
                    m.Value = inc.Value;
                    m.Quantity = inc.Quantity;
                }
            }
            ai.Save();
            //save the MU in materials
            var mat = am.Materials.Find(x => x.Id == inc.IdMaterial);
            if (mat.CurrentMU != inc.MU)
            {
                mat.CurrentMU = inc.MU;
                am.Save();
            }
            this.Close();
        }

        private void CmbMaterial_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CmbMaterial.SelectedIndex != -1)
            {
                var m = am.Materials.Find(x => x.Id == GetMaterialId(CmbMaterial.Text));
                TxtMU.Text = m.CurrentMU.ToString("0.00");
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
