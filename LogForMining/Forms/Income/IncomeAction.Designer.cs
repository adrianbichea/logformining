﻿namespace LogForMining.Forms.Income
{
    partial class IncomeAction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IncomeAction));
            this.GrpBox = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LblMU = new System.Windows.Forms.Label();
            this.TxtMU = new System.Windows.Forms.TextBox();
            this.LblValue = new System.Windows.Forms.Label();
            this.TxtValue = new System.Windows.Forms.TextBox();
            this.LblQuant = new System.Windows.Forms.Label();
            this.TxtQuantity = new System.Windows.Forms.TextBox();
            this.CmbMaterial = new System.Windows.Forms.ComboBox();
            this.LblMat = new System.Windows.Forms.Label();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.GrpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpBox
            // 
            this.GrpBox.Controls.Add(this.label5);
            this.GrpBox.Controls.Add(this.LblMU);
            this.GrpBox.Controls.Add(this.TxtMU);
            this.GrpBox.Controls.Add(this.LblValue);
            this.GrpBox.Controls.Add(this.TxtValue);
            this.GrpBox.Controls.Add(this.LblQuant);
            this.GrpBox.Controls.Add(this.TxtQuantity);
            this.GrpBox.Controls.Add(this.CmbMaterial);
            this.GrpBox.Controls.Add(this.LblMat);
            this.GrpBox.Location = new System.Drawing.Point(12, 12);
            this.GrpBox.Name = "GrpBox";
            this.GrpBox.Size = new System.Drawing.Size(271, 154);
            this.GrpBox.TabIndex = 0;
            this.GrpBox.TabStop = false;
            this.GrpBox.Text = "Income";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(181, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "PED";
            // 
            // LblMU
            // 
            this.LblMU.AutoSize = true;
            this.LblMU.Location = new System.Drawing.Point(6, 118);
            this.LblMU.Name = "LblMU";
            this.LblMU.Size = new System.Drawing.Size(21, 15);
            this.LblMU.TabIndex = 7;
            this.LblMU.Text = "MU";
            // 
            // TxtMU
            // 
            this.TxtMU.Location = new System.Drawing.Point(75, 115);
            this.TxtMU.Name = "TxtMU";
            this.TxtMU.Size = new System.Drawing.Size(100, 23);
            this.TxtMU.TabIndex = 6;
            // 
            // LblValue
            // 
            this.LblValue.AutoSize = true;
            this.LblValue.Location = new System.Drawing.Point(6, 89);
            this.LblValue.Name = "LblValue";
            this.LblValue.Size = new System.Drawing.Size(42, 15);
            this.LblValue.TabIndex = 5;
            this.LblValue.Text = "Value";
            // 
            // TxtValue
            // 
            this.TxtValue.Location = new System.Drawing.Point(75, 86);
            this.TxtValue.Name = "TxtValue";
            this.TxtValue.Size = new System.Drawing.Size(100, 23);
            this.TxtValue.TabIndex = 4;
            this.TxtValue.TextChanged += new System.EventHandler(this.TxtValue_TextChanged);
            // 
            // LblQuant
            // 
            this.LblQuant.AutoSize = true;
            this.LblQuant.Location = new System.Drawing.Point(6, 60);
            this.LblQuant.Name = "LblQuant";
            this.LblQuant.Size = new System.Drawing.Size(63, 15);
            this.LblQuant.TabIndex = 3;
            this.LblQuant.Text = "Quantity";
            // 
            // TxtQuantity
            // 
            this.TxtQuantity.Location = new System.Drawing.Point(75, 57);
            this.TxtQuantity.Name = "TxtQuantity";
            this.TxtQuantity.Size = new System.Drawing.Size(100, 23);
            this.TxtQuantity.TabIndex = 2;
            this.TxtQuantity.TextChanged += new System.EventHandler(this.TxtQuantity_TextChanged);
            // 
            // CmbMaterial
            // 
            this.CmbMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMaterial.FormattingEnabled = true;
            this.CmbMaterial.Location = new System.Drawing.Point(75, 28);
            this.CmbMaterial.Name = "CmbMaterial";
            this.CmbMaterial.Size = new System.Drawing.Size(186, 23);
            this.CmbMaterial.TabIndex = 1;
            this.CmbMaterial.SelectedIndexChanged += new System.EventHandler(this.CmbMaterial_SelectedIndexChanged);
            // 
            // LblMat
            // 
            this.LblMat.AutoSize = true;
            this.LblMat.Location = new System.Drawing.Point(6, 31);
            this.LblMat.Name = "LblMat";
            this.LblMat.Size = new System.Drawing.Size(63, 15);
            this.LblMat.TabIndex = 0;
            this.LblMat.Text = "Material";
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(127, 172);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 7;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(208, 172);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 8;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // IncomeAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 203);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GrpBox);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IncomeAction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IncomeAction";
            this.Load += new System.EventHandler(this.IncomeAction_Load);
            this.GrpBox.ResumeLayout(false);
            this.GrpBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBox;
        private System.Windows.Forms.Label LblMat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblMU;
        private System.Windows.Forms.TextBox TxtMU;
        private System.Windows.Forms.Label LblValue;
        private System.Windows.Forms.TextBox TxtValue;
        private System.Windows.Forms.Label LblQuant;
        private System.Windows.Forms.TextBox TxtQuantity;
        private System.Windows.Forms.ComboBox CmbMaterial;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnCancel;
    }
}