﻿namespace LogForMining.Forms.Income
{
    partial class IncomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IncomeForm));
            this.PanelUp = new System.Windows.Forms.Panel();
            this.BtnRemove = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.BtnModif = new System.Windows.Forms.Button();
            this.BtnNew = new System.Windows.Forms.Button();
            this.PanelDown = new System.Windows.Forms.Panel();
            this.PgUp = new System.Windows.Forms.Button();
            this.PgDown = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.PanelCenter = new System.Windows.Forms.Panel();
            this.Table = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdRound = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdMaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAfterMU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelUp.SuspendLayout();
            this.PanelDown.SuspendLayout();
            this.PanelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelUp
            // 
            this.PanelUp.Controls.Add(this.BtnRemove);
            this.PanelUp.Controls.Add(this.BtnModif);
            this.PanelUp.Controls.Add(this.BtnNew);
            this.PanelUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelUp.Location = new System.Drawing.Point(0, 0);
            this.PanelUp.Name = "PanelUp";
            this.PanelUp.Size = new System.Drawing.Size(569, 78);
            this.PanelUp.TabIndex = 0;
            // 
            // BtnRemove
            // 
            this.BtnRemove.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRemove.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnRemove.ImageIndex = 2;
            this.BtnRemove.ImageList = this.imageList1;
            this.BtnRemove.Location = new System.Drawing.Point(374, 12);
            this.BtnRemove.Name = "BtnRemove";
            this.BtnRemove.Size = new System.Drawing.Size(175, 60);
            this.BtnRemove.TabIndex = 5;
            this.BtnRemove.Text = "Remove";
            this.BtnRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnRemove.UseVisualStyleBackColor = true;
            this.BtnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Iron-Ingot-icon.png");
            this.imageList1.Images.SetKeyName(1, "Edit-Document-icon.png");
            this.imageList1.Images.SetKeyName(2, "remove-from-database-icon.png");
            // 
            // BtnModif
            // 
            this.BtnModif.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnModif.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnModif.ImageIndex = 1;
            this.BtnModif.ImageList = this.imageList1;
            this.BtnModif.Location = new System.Drawing.Point(193, 12);
            this.BtnModif.Name = "BtnModif";
            this.BtnModif.Size = new System.Drawing.Size(175, 60);
            this.BtnModif.TabIndex = 4;
            this.BtnModif.Text = "Modify";
            this.BtnModif.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnModif.UseVisualStyleBackColor = true;
            this.BtnModif.Click += new System.EventHandler(this.BtnModif_Click);
            // 
            // BtnNew
            // 
            this.BtnNew.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNew.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnNew.ImageIndex = 0;
            this.BtnNew.ImageList = this.imageList1;
            this.BtnNew.Location = new System.Drawing.Point(12, 12);
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.Size = new System.Drawing.Size(175, 60);
            this.BtnNew.TabIndex = 3;
            this.BtnNew.Text = "New";
            this.BtnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnNew.UseVisualStyleBackColor = true;
            this.BtnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // PanelDown
            // 
            this.PanelDown.Controls.Add(this.PgUp);
            this.PanelDown.Controls.Add(this.PgDown);
            this.PanelDown.Controls.Add(this.lblPage);
            this.PanelDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelDown.Location = new System.Drawing.Point(0, 481);
            this.PanelDown.Name = "PanelDown";
            this.PanelDown.Size = new System.Drawing.Size(569, 41);
            this.PanelDown.TabIndex = 1;
            // 
            // PgUp
            // 
            this.PgUp.Location = new System.Drawing.Point(180, 9);
            this.PgUp.Name = "PgUp";
            this.PgUp.Size = new System.Drawing.Size(52, 23);
            this.PgUp.TabIndex = 8;
            this.PgUp.Text = "<<";
            this.PgUp.UseVisualStyleBackColor = true;
            this.PgUp.Click += new System.EventHandler(this.PgUp_Click);
            // 
            // PgDown
            // 
            this.PgDown.Location = new System.Drawing.Point(336, 9);
            this.PgDown.Name = "PgDown";
            this.PgDown.Size = new System.Drawing.Size(52, 23);
            this.PgDown.TabIndex = 7;
            this.PgDown.Text = ">>";
            this.PgDown.UseVisualStyleBackColor = true;
            this.PgDown.Click += new System.EventHandler(this.PgDown_Click);
            // 
            // lblPage
            // 
            this.lblPage.Location = new System.Drawing.Point(238, 9);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(92, 23);
            this.lblPage.TabIndex = 6;
            this.lblPage.Text = "label1";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PanelCenter
            // 
            this.PanelCenter.Controls.Add(this.Table);
            this.PanelCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelCenter.Location = new System.Drawing.Point(0, 78);
            this.PanelCenter.Name = "PanelCenter";
            this.PanelCenter.Size = new System.Drawing.Size(569, 403);
            this.PanelCenter.TabIndex = 2;
            // 
            // Table
            // 
            this.Table.AllowUserToAddRows = false;
            this.Table.AllowUserToDeleteRows = false;
            this.Table.AllowUserToResizeColumns = false;
            this.Table.AllowUserToResizeRows = false;
            this.Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colIdRound,
            this.colIdMaterial,
            this.colMaterial,
            this.colQuantity,
            this.colValue,
            this.colMU,
            this.colAfterMU});
            this.Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Table.Location = new System.Drawing.Point(0, 0);
            this.Table.MultiSelect = false;
            this.Table.Name = "Table";
            this.Table.ReadOnly = true;
            this.Table.RowHeadersVisible = false;
            this.Table.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Table.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Table.Size = new System.Drawing.Size(569, 403);
            this.Table.TabIndex = 1;
            this.Table.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table_CellDoubleClick);
            // 
            // colId
            // 
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Width = 40;
            // 
            // colIdRound
            // 
            this.colIdRound.HeaderText = "IdRound";
            this.colIdRound.Name = "colIdRound";
            this.colIdRound.ReadOnly = true;
            this.colIdRound.Visible = false;
            // 
            // colIdMaterial
            // 
            this.colIdMaterial.HeaderText = "IdMaterial";
            this.colIdMaterial.Name = "colIdMaterial";
            this.colIdMaterial.ReadOnly = true;
            this.colIdMaterial.Visible = false;
            // 
            // colMaterial
            // 
            this.colMaterial.HeaderText = "Material";
            this.colMaterial.Name = "colMaterial";
            this.colMaterial.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.HeaderText = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colValue
            // 
            this.colValue.HeaderText = "Value";
            this.colValue.Name = "colValue";
            this.colValue.ReadOnly = true;
            // 
            // colMU
            // 
            this.colMU.HeaderText = "MU";
            this.colMU.Name = "colMU";
            this.colMU.ReadOnly = true;
            // 
            // colAfterMU
            // 
            this.colAfterMU.HeaderText = "AfterMU";
            this.colAfterMU.Name = "colAfterMU";
            this.colAfterMU.ReadOnly = true;
            // 
            // IncomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 522);
            this.Controls.Add(this.PanelCenter);
            this.Controls.Add(this.PanelDown);
            this.Controls.Add(this.PanelUp);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IncomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IncomeForm";
            this.Load += new System.EventHandler(this.IncomeForm_Load);
            this.PanelUp.ResumeLayout(false);
            this.PanelDown.ResumeLayout(false);
            this.PanelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelUp;
        private System.Windows.Forms.Panel PanelDown;
        private System.Windows.Forms.Panel PanelCenter;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button BtnRemove;
        private System.Windows.Forms.Button BtnModif;
        private System.Windows.Forms.Button BtnNew;
        private System.Windows.Forms.Button PgUp;
        private System.Windows.Forms.Button PgDown;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.DataGridView Table;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdRound;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdMaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMU;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAfterMU;
    }
}