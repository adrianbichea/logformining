﻿using LogForMining.DB.Income;
using LogForMining.DB.Material;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining.Forms.Income
{
    public partial class IncomeForm : Form
    {
        int maxPages { get; set; }
        int currentPage { get; set; }
        int lastIndexClicked { get; set; }

        long idRound { get; set; }
        AllMaterials am { get; set; }

        public IncomeForm(long idRound)
        {
            InitializeComponent();
            this.idRound = idRound;
        }

        private void IncomeForm_Load(object sender, EventArgs e)
        {
            ShowLang();
            Utils.Utils.SetDoubleBuffered(Table);
            currentPage = 1;
            am = AllMaterials.Load();
            ShowTable();
            foreach (DataGridViewColumn column in Table.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }            
        }

        private void ButtonPages()
        {
            PgDown.Enabled = true;
            PgUp.Enabled = true;
            if (currentPage == 1)
            {
                PgUp.Enabled = false;
            }
            if (currentPage == maxPages || maxPages == 0)
            {
                PgDown.Enabled = false;
            }
        }

        private void ShowLang()
        {
            Text = Utils.Language.GetText("{IncomeForm}");
            BtnNew.Text = Utils.Language.GetText("{BtnNew}");
            BtnModif.Text = Utils.Language.GetText("{BtnModify}");
            BtnRemove.Text = Utils.Language.GetText("{BtnDelete}");
            colId.HeaderText = Utils.Language.GetText("{Id}");
            colMaterial.HeaderText = Utils.Language.GetText("{Material}");
            colQuantity.HeaderText = Utils.Language.GetText("{Quantity}");
            colValue.HeaderText = Utils.Language.GetText("{TTValue}");
            colMU.HeaderText = Utils.Language.GetText("{MU}");
            colAfterMU.HeaderText = Utils.Language.GetText("{AfterMU}");
        }

        private void ShowTable()
        {
            AllIncomes ai = AllIncomes.Load();
            if (ai == null)
            {
                ButtonPages();
                return;
            }
            Table.Rows.Clear();
            var list = ai.Incomes.FindAll(a=>a.IdRound == idRound).OrderByDescending(x => x.Id).ToList();

            var itemPerPage = Utils.Utils.itemPerPage;
            maxPages = (int)Math.Ceiling((double)list.Count / (double)itemPerPage);
            if (currentPage > maxPages && maxPages > 0)
            {
                currentPage = maxPages;
            }
            var lastPageCount = list.Count % itemPerPage;
            if (list.Count > 0 && lastPageCount == 0)
            {
                lastPageCount = itemPerPage;
            }
            list = list.GetRange((currentPage - 1) * itemPerPage, currentPage < maxPages ? itemPerPage : lastPageCount);
            lblPage.Text = currentPage.ToString() + " / " + maxPages.ToString();
            foreach (var a in list)
            {
                Table.Rows.Add();
                var row = (DataGridViewRow)Table.Rows[Table.Rows.Count - 1];
                row.Cells[colId.Index].Value = a.Id.ToString();
                var mat = am.Materials.Find(x => x.Id == a.IdMaterial);
                row.Cells[colMaterial.Index].Value = mat.Name;
                double val = 0;
                if (a.Quantity != -1)
                {
                    row.Cells[colQuantity.Index].Value = a.Quantity.ToString();
                    val = a.Quantity * mat.TTValue;
                }
                else
                {
                    val = a.Value;
                }
                row.Cells[colValue.Index].Value = val.ToString("0.00000");
                row.Cells[colMU.Index].Value = a.MU.ToString("0.00");
                row.Cells[colAfterMU.Index].Value = (a.MU * 0.01 * val).ToString("0.00");

                row.Cells[colIdRound.Index].Value = a.IdRound.ToString();
                row.Cells[colIdMaterial.Index].Value = a.IdMaterial.ToString();
            }
            if (list.Count > 0)
            {
                Table.Rows[lastIndexClicked].Selected = true;
            }
            ButtonPages();
        }

        private void PgUp_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                ShowTable();
            }
        }

        private void PgDown_Click(object sender, EventArgs e)
        {
            if (currentPage < maxPages)
            {
                currentPage++;
                ShowTable();
            }
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            IncomeAction ia = new IncomeAction(idRound);
            ia.ShowDialog();
            ShowTable();
        }

        private void BtnModif_Click(object sender, EventArgs e)
        {
            if (Table.SelectedRows.Count < 1)
            {
                return;
            }
            lastIndexClicked = Table.SelectedRows[0].Index;
            var dr = GetDataIncome(Table.SelectedRows[0]);
            IncomeAction ma = new IncomeAction(dr.IdRound, dr);
            ma.ShowDialog();
            ShowTable();
        }

        private DB.Income.Income GetDataIncome(DataGridViewRow r)
        {
            var dr = new DB.Income.Income();
            dr.Id = int.Parse(r.Cells[colId.Index].Value.ToString());
            dr.IdMaterial = int.Parse(r.Cells[colIdMaterial.Index].Value.ToString());
            dr.IdRound = int.Parse(r.Cells[colIdRound.Index].Value.ToString());
            dr.MU = double.Parse(r.Cells[colMU.Index].Value.ToString());
            if (r.Cells[colQuantity.Index].Value != null && !string.IsNullOrEmpty(r.Cells[colQuantity.Index].Value.ToString()))
            {
                dr.Quantity = long.Parse(r.Cells[colQuantity.Index].Value.ToString());
            }
            else
            {
                dr.Quantity = -1;
            }
            dr.Value = double.Parse(r.Cells[colValue.Index].Value.ToString());
            return dr;
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            if (Table.SelectedRows.Count < 1)
            {
                return;
            }
            int index = int.Parse(Table.SelectedRows[0].Cells[colId.Index].Value.ToString());
            if (MessageBox.Show($"Do you want to delete this field? (Id: {index})", "Really?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AllIncomes ai = AllIncomes.Load();
                var m = ai.Incomes.Find(x => x.Id == index);
                if (m != null)
                {
                    ai.Incomes.Remove(m);
                    ai.Save();
                    ShowTable();
                }
                else
                {
                    MessageBox.Show("Something went wrong!");
                }
            }
        }

        private void Table_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnModif_Click(null, null);
        }
    }
}
