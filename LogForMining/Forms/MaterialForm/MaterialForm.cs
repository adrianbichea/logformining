﻿using LogForMining.DB.Material;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace LogForMining.Forms.MaterialForm
{
    public partial class MaterialForm : Form
    {
        int maxPages { get; set; }
        int currentPage { get; set; }
        int lastIndexClicked { get; set; }
        MaterialType mt { get; set; }

        public MaterialForm()
        {
            InitializeComponent();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            MaterialAction ma = new MaterialAction();
            ma.ShowDialog();
            ShowTable();
        }

        private void MaterialForm_Load(object sender, EventArgs e)
        {
            mt = MaterialType.Load();
            ShowLang();
            currentPage = 1;
            Utils.Utils.SetDoubleBuffered(Table);
            ShowTable();
            foreach (DataGridViewColumn column in Table.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void ShowLang()
        {
            BtnNew.Text = Utils.Language.GetText("{BtnNew}");
            BtnModif.Text = Utils.Language.GetText("{BtnModify}");
            BtnRemove.Text = Utils.Language.GetText("{BtnDelete}");
            colId.HeaderText = Utils.Language.GetText("{Id}");
            colName.HeaderText = Utils.Language.GetText("{Name}");
            colTTValue.HeaderText = Utils.Language.GetText("{TTValue}");
            colCurrentMu.HeaderText = Utils.Language.GetText("{CurrentMU}");
            colType.HeaderText = Utils.Language.GetText("{MatType}");
        }

        private void ButtonPages()
        {
            PgDown.Enabled = true;
            PgUp.Enabled = true;
            if (currentPage == 1)
            {
                PgUp.Enabled = false;
            }
            if (currentPage == maxPages || maxPages == 0)
            {
                PgDown.Enabled = false;
            }
        }

        private void ShowTable()
        {

            AllMaterials am = AllMaterials.Load();
            if (am == null)
            {
                ButtonPages();
                return;
            }
            Table.Rows.Clear();
            var list = am.Materials.OrderByDescending(x => x.Id).ToList();

            var itemPerPage = Utils.Utils.itemPerPage;
            maxPages = (int)Math.Ceiling((double)list.Count / (double)itemPerPage);
            if (currentPage > maxPages && maxPages > 0)
            {
                currentPage = maxPages;
            }
            var lastPageCount = list.Count % itemPerPage;
            if (list.Count > 0 && lastPageCount == 0)
            {
                lastPageCount = itemPerPage;
            }
            list = list.GetRange((currentPage - 1) * itemPerPage, currentPage < maxPages ? itemPerPage : lastPageCount);
            lblPage.Text = currentPage.ToString() + " / " + maxPages.ToString();
            foreach (var a in list)
            {
                Table.Rows.Add();
                var row = (DataGridViewRow)Table.Rows[Table.Rows.Count - 1];
                row.Cells[colId.Index].Value = a.Id.ToString();
                row.Cells[colName.Index].Value = a.Name;
                row.Cells[colTTValue.Index].Value = a.TTValue.ToString("0.00000");
                row.Cells[colCurrentMu.Index].Value = a.CurrentMU.ToString("0.00000");
                row.Cells[colType.Index].Value = mt.GetMaterial(a.MaterialType);
            }
            if (list.Count > 0)
            {
                Table.Rows[lastIndexClicked].Selected = true;
            }
            ButtonPages();
        }

        private void PgUp_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                ShowTable();
            }
        }

        private void PgDown_Click(object sender, EventArgs e)
        {
            if (currentPage < maxPages)
            {
                currentPage++;
                ShowTable();
            }
        }

        private void BtnModif_Click(object sender, EventArgs e)
        {
            if (Table.SelectedRows.Count < 1)
            {
                return;
            }
            lastIndexClicked = Table.SelectedRows[0].Index;
            var dr = GetDataMaterial(Table.SelectedRows[0]);
            MaterialAction ma = new MaterialAction(dr);
            ma.ShowDialog();
            ShowTable();
        }

        private Material GetDataMaterial(DataGridViewRow r)
        {
            Material dr = new Material();
            dr.Id = int.Parse(r.Cells[colId.Index].Value.ToString());
            dr.Name = r.Cells[colName.Index].Value.ToString();
            dr.TTValue = double.Parse(r.Cells[colTTValue.Index].Value.ToString());
            dr.CurrentMU = double.Parse(r.Cells[colCurrentMu.Index].Value.ToString());
            dr.MaterialType = GetMaterialType(r.Cells[colType.Index].Value.ToString());
            return dr;
        }

        private int GetMaterialType(string name)
        {
            return mt.MaterialsType.FindIndex(x => x == name);
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            if (Table.SelectedRows.Count < 1)
            {
                return;
            }
            int index = int.Parse(Table.SelectedRows[0].Cells[colId.Index].Value.ToString());
            if (MessageBox.Show($"Do you want to delete this field? (Id: {index})", "Really?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AllMaterials am = AllMaterials.Load();
                var m = am.Materials.Find(x => x.Id == index);
                if (m != null)
                {
                    am.Materials.Remove(m);
                    am.Save();
                    ShowTable();
                }
                else
                {
                    MessageBox.Show("Something went wrong!");
                }
            }
        }

        private void Table_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnModif_Click(null, null);
        }
    }
}
