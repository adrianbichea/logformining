﻿using LogForMining.DB.Material;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogForMining.Forms.MaterialForm
{
    public partial class MaterialAction : Form
    {
        Material material { get; set; }

        public MaterialAction(Material material = null)
        {
            InitializeComponent();
            this.material = null;
            if (material != null)
            {
                this.material = material;
            }
        }

        private void MaterialAction_Load(object sender, EventArgs e)
        {
            MaterialType mt = MaterialType.Load();
            foreach (var s in mt.MaterialsType)
            {
                CmbMatType.Items.Add(s);
            }
            ShowLang();
            if (material != null)
            {
                TxtMaterialName.Text = material.Name;
                TxtTTValue.Text = String.Format("{0:F5}", material.TTValue); //material.TTValue.ToString("R");
                TxtMU.Text = String.Format("{0:F2}", material.CurrentMU); //material.CurrentMU.ToString("R");
                CmbMatType.SelectedIndex = material.MaterialType;
                Text = "Modify material";
            }
            else
            {
                Text = "New material";
            }
        }

        private void ShowLang()
        {
            Text = Utils.Language.GetText("{MaterialFormText}");
            GrpBox.Text = Utils.Language.GetText("{InfoMaterial}");
            BtnOk.Text = Utils.Language.GetText("{Ok}");
            BtnCancel.Text = Utils.Language.GetText("{Cancel}");
            LblName.Text = Utils.Language.GetText("{Name}");
            LblTTVal.Text = Utils.Language.GetText("{TTValue}");
            LblCurMU.Text = Utils.Language.GetText("{CurrentMU}");
            LblMatType.Text = Utils.Language.GetText("{MatType}");
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            material = null;
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (material != null)
            {
                if (SetMaterialFromForm(material.Id))
                {
                    Save(false);
                }
            }
            else
            {
                if (SetMaterialFromForm())
                {
                    Save();
                }
            }
        }

        private void Save(bool newMaterial = true)
        {
            AllMaterials am = AllMaterials.Load();
            if (newMaterial)
            {
                material.Id = ++am.LastId;
                am.Materials.Add(material);
            }
            else
            {
                var m = am.Materials.Find(x => x.Id == material.Id);
                if (m != null)
                {
                    m.Id = material.Id;
                    m.CurrentMU = material.CurrentMU;
                    m.MaterialType = material.MaterialType;
                    m.Name = material.Name;
                    m.TTValue = material.TTValue;
                }
            }
            am.Save();
            this.Close();
        }

        private bool SetMaterialFromForm(long id = -1)
        {
            if (string.IsNullOrEmpty(TxtMaterialName.Text))
            {
                MessageBox.Show("Material name is empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (material == null && !IsMaterialNameUnique(TxtMaterialName.Text))
            {
                MessageBox.Show("Material name already defined", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (CmbMatType.SelectedIndex == -1)
            {
                MessageBox.Show("Material type not selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            double tt = 0;
            double mu = 0;
            if (!double.TryParse(TxtTTValue.Text, out tt))
            {
                MessageBox.Show("TT is not a value (numbers only)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (!double.TryParse(TxtMU.Text, out mu))
            {
                MessageBox.Show("MU is not a value (numbers only)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            material = new Material()
            {
                Id = id,
                Name = TxtMaterialName.Text,
                CurrentMU = mu,
                MaterialType = CmbMatType.SelectedIndex,
                TTValue = tt
            };
            return true;
        }

        private bool IsMaterialNameUnique(string matName)
        {
            AllMaterials am = AllMaterials.Load();
            if (am.Materials.Find(x => x.Name == matName) != null)
            {
                return false;
            }
            return true;
        }

        public Material GetMaterial()
        {
            return material;
        }
    }
}
