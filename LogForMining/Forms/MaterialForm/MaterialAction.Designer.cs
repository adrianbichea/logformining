﻿namespace LogForMining.Forms.MaterialForm
{
    partial class MaterialAction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaterialAction));
            this.GrpBox = new System.Windows.Forms.GroupBox();
            this.LblMatType = new System.Windows.Forms.Label();
            this.CmbMatType = new System.Windows.Forms.ComboBox();
            this.TxtMU = new System.Windows.Forms.TextBox();
            this.LblCurMU = new System.Windows.Forms.Label();
            this.TxtTTValue = new System.Windows.Forms.TextBox();
            this.LblTTVal = new System.Windows.Forms.Label();
            this.TxtMaterialName = new System.Windows.Forms.TextBox();
            this.LblName = new System.Windows.Forms.Label();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.GrpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpBox
            // 
            this.GrpBox.Controls.Add(this.LblMatType);
            this.GrpBox.Controls.Add(this.CmbMatType);
            this.GrpBox.Controls.Add(this.TxtMU);
            this.GrpBox.Controls.Add(this.LblCurMU);
            this.GrpBox.Controls.Add(this.TxtTTValue);
            this.GrpBox.Controls.Add(this.LblTTVal);
            this.GrpBox.Controls.Add(this.TxtMaterialName);
            this.GrpBox.Controls.Add(this.LblName);
            this.GrpBox.Location = new System.Drawing.Point(12, 12);
            this.GrpBox.Name = "GrpBox";
            this.GrpBox.Size = new System.Drawing.Size(288, 162);
            this.GrpBox.TabIndex = 0;
            this.GrpBox.TabStop = false;
            this.GrpBox.Text = "Info Material";
            // 
            // LblMatType
            // 
            this.LblMatType.AutoSize = true;
            this.LblMatType.Location = new System.Drawing.Point(12, 125);
            this.LblMatType.Name = "LblMatType";
            this.LblMatType.Size = new System.Drawing.Size(98, 15);
            this.LblMatType.TabIndex = 7;
            this.LblMatType.Text = "Material type";
            // 
            // CmbMatType
            // 
            this.CmbMatType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMatType.FormattingEnabled = true;
            this.CmbMatType.Location = new System.Drawing.Point(116, 122);
            this.CmbMatType.Name = "CmbMatType";
            this.CmbMatType.Size = new System.Drawing.Size(157, 23);
            this.CmbMatType.TabIndex = 4;
            // 
            // TxtMU
            // 
            this.TxtMU.Location = new System.Drawing.Point(95, 95);
            this.TxtMU.Name = "TxtMU";
            this.TxtMU.Size = new System.Drawing.Size(178, 23);
            this.TxtMU.TabIndex = 3;
            // 
            // LblCurMU
            // 
            this.LblCurMU.AutoSize = true;
            this.LblCurMU.Location = new System.Drawing.Point(12, 98);
            this.LblCurMU.Name = "LblCurMU";
            this.LblCurMU.Size = new System.Drawing.Size(77, 15);
            this.LblCurMU.TabIndex = 4;
            this.LblCurMU.Text = "Current MU";
            // 
            // TxtTTValue
            // 
            this.TxtTTValue.Location = new System.Drawing.Point(95, 66);
            this.TxtTTValue.Name = "TxtTTValue";
            this.TxtTTValue.Size = new System.Drawing.Size(178, 23);
            this.TxtTTValue.TabIndex = 2;
            // 
            // LblTTVal
            // 
            this.LblTTVal.AutoSize = true;
            this.LblTTVal.Location = new System.Drawing.Point(12, 69);
            this.LblTTVal.Name = "LblTTVal";
            this.LblTTVal.Size = new System.Drawing.Size(63, 15);
            this.LblTTVal.TabIndex = 2;
            this.LblTTVal.Text = "TT Value";
            // 
            // TxtMaterialName
            // 
            this.TxtMaterialName.Location = new System.Drawing.Point(95, 37);
            this.TxtMaterialName.Name = "TxtMaterialName";
            this.TxtMaterialName.Size = new System.Drawing.Size(178, 23);
            this.TxtMaterialName.TabIndex = 1;
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(12, 40);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(35, 15);
            this.LblName.TabIndex = 0;
            this.LblName.Text = "Name";
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(225, 180);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 6;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(144, 180);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 5;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // MaterialAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 211);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GrpBox);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MaterialAction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MaterialAction";
            this.Load += new System.EventHandler(this.MaterialAction_Load);
            this.GrpBox.ResumeLayout(false);
            this.GrpBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBox;
        private System.Windows.Forms.TextBox TxtMaterialName;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.TextBox TxtMU;
        private System.Windows.Forms.Label LblCurMU;
        private System.Windows.Forms.TextBox TxtTTValue;
        private System.Windows.Forms.Label LblTTVal;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Label LblMatType;
        private System.Windows.Forms.ComboBox CmbMatType;
    }
}