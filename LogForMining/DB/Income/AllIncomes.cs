﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Income
{
    public class AllIncomes : XmlSerializable<AllIncomes>
    {
        const string FILENAME = "income.xml";

        [XmlAttribute("LastId")]
        public long LastId { get; set; }

        [XmlElement("Incomes")]
        public List<Income> Incomes { get; set; }

        public AllIncomes()
        {
            LastId = 0;
            Incomes = new List<Income>();
        }

        public static AllIncomes Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
