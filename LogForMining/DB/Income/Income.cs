﻿using System.Xml.Serialization;

namespace LogForMining.DB.Income
{
    public class Income
    {
        [XmlAttribute("Id")]
        public long Id { get; set; }

        [XmlAttribute("IdRound")]
        public long IdRound { get; set; }

        [XmlAttribute("IdMaterial")]
        public long IdMaterial { get; set; }

        [XmlAttribute("Quantity")]
        public long Quantity { get; set; }

        [XmlAttribute("Value")]
        public double Value { get; set; }

        [XmlAttribute("MU")]
        public double MU { get; set; }
    }
}
