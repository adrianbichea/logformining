﻿using System.Xml.Serialization;

namespace LogForMining.DB.Sell
{
    public class Sell
    {
        [XmlAttribute("Id")]
        public long Id { get; set; }

        [XmlAttribute("IdMaterial")]
        public long IdMaterial { get; set; }

        [XmlAttribute("Quantity")]
        public long Quantity { get; set; }

        [XmlAttribute("ValueSold")]
        public double ValueSold { get; set; }
    }
}
