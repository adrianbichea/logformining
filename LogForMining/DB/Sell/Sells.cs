﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Sell
{
    public class Sells : XmlSerializable<Sells>
    {
        const string FILENAME = "sell.xml";

        [XmlAttribute("LastId")]
        public long LastId { get; set; }

        [XmlElement("Sells")]
        public List<Sell> SellItems { get; set; }

        public Sells()
        {
            LastId = 0;
            SellItems = new List<DB.Sell.Sell>();
        }

        public static Sells Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
