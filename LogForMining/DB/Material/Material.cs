﻿using System.Xml.Serialization;

namespace LogForMining.DB.Material
{
    public class Material
    {
        [XmlAttribute("Id")]
        public long Id { get; set; }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("TTValue")]
        public double TTValue { get; set; }

        [XmlAttribute("CurrentMU")]
        public double CurrentMU { get; set; }

        [XmlAttribute("MaterialType")] //to use it in refine :) .. 0 - ore, 1 - emn, 2 - treasure
        public int MaterialType { get; set; }
    }
}
