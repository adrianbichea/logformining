﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Material
{
    public class AllMaterials : XmlSerializable<AllMaterials>
    {
        const string FILENAME = "materials.xml";

        [XmlAttribute("LastId")]
        public long LastId { get; set; }

        [XmlElement("Materials")]
        public List<Material> Materials { get; set; }

        public AllMaterials()
        {
            LastId = 0;
            Materials = new List<Material>();
        }

        public static AllMaterials Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
