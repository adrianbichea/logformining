﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Material
{
    public class MaterialType : XmlSerializable<MaterialType>
    {
        const string FILENAME = "materialstype.xml";
        [XmlElement("MaterialsType")]
        public List<string> MaterialsType { get; set; }

        public MaterialType()
        {
            MaterialsType = new List<string>();
        }

        public static MaterialType Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }

        public string GetMaterial(int index)
        {
            if (index < 0 || index >= MaterialsType.Count)
            {
                return "ERROR";
            }
            return MaterialsType[index];
        }
    }
}
