﻿using System.IO;
using System.Xml.Serialization;

namespace MiningLog.DB
{
    public class XmlSerializable<T> where T : new()
    {
        //Method to save the T to XML file
        public void SaveToXML(string filename) 
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
            TextWriter tw = new StreamWriter(filename);
            xs.Serialize(tw, this);
            tw.Close();
        }

        //Static method to load the filename into T
        public static T ReadFromXML(string filename)
        {
            if (!File.Exists(filename)) //if the file is not found
            {
                return new T();//send a new T
            }
            XmlSerializer xs = new XmlSerializer(typeof(T));
            using (var sr = new StreamReader(filename))
            {
                var op = (T)xs.Deserialize(sr);
                sr.Close();
                return op;
            }
        }
    }
}
