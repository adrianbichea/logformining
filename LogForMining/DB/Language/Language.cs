﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LogForMining.DB.Language
{
    public class Language
    {
        [XmlAttribute("LanguageName")]
        public string LangName { get; set; }

        [XmlAttribute("LanguageFile")]
        public string LangFile { get; set; }
    }
}
