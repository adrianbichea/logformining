﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Language
{
    public class LanguageLang : XmlSerializable<LanguageLang>
    {      
        [XmlElement("Language")]
        public List<LanguageText> LanguageText { get; set; }

        public LanguageLang()
        {
            LanguageText = new List<LanguageText>();
        }
    }
}
