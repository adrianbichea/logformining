﻿using MiningLog.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LogForMining.DB.Language
{
    public class AllLanguages : XmlSerializable<AllLanguages>
    {
        const string FILENAME = "language.xml";

        [XmlAttribute("CurrentLanguage")]
        public int CurrentLanguage { get; set; }

        [XmlElement("AllLanguages")]
        public List<Language> Languages { get; set; }

        public AllLanguages()
        {
            Languages = new List<Language>();
        }

        public static AllLanguages Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
