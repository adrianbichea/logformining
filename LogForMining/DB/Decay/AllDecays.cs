﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Decay
{
    public class AllDecays : XmlSerializable<AllDecays>
    {
        const string FILENAME = "decay.xml";

        [XmlAttribute("LastId")]
        public long LastId { get; set; }

        [XmlElement("Decays")]
        public List<Decay> Decays { get; set; }

        public AllDecays()
        {
            LastId = 0;
            Decays = new List<Decay>();
        }

        public static AllDecays Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
