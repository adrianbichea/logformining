﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForEu.DB.Decay
{
    public class DecayType : XmlSerializable<DecayType>
    {
        const string FILENAME = "decaytype.xml";
        [XmlElement("DecayType")]
        public List<string> DecaysType { get; set; }

        public DecayType()
        {
            DecaysType = new List<string>();
        }

        public static DecayType Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }

        public string GetMaterial(int index)
        {
            if (index < 0 || index >= DecaysType.Count)
            {
                return "ERROR";
            }
            return DecaysType[index];
        }
    }
}
