﻿using System.Xml.Serialization;

namespace LogForMining.DB.Decay
{
    public class Decay
    {
        [XmlAttribute("Id")]
        public long Id { get; set; }

        //is the same as Id from Round
        [XmlAttribute("IdRound")]
        public long IdRound { get; set; }

        [XmlAttribute("DecayType")]
        public int DecayType  { get; set; }

        [XmlAttribute("Value")]
        public double Value { get; set; }
    }
}
