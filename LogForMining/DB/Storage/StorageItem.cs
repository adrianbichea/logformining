﻿using System.Xml.Serialization;

namespace LogForMining.DB.Storage
{
    public class StorageItem
    {
        [XmlAttribute("Id")]
        public long Id { get; set; }

        [XmlAttribute("IdMaterial")]
        public long IdMaterial { get; set; }

        [XmlAttribute("Quantity")]
        public long Quantity { get; set; }
    }
}
