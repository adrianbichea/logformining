﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Storage
{
    public class Storage : XmlSerializable<Storage>
    {
        const string FILENAME = "storage.xml";

        [XmlAttribute("LastId")]
        public long LastId { get; set; }

        [XmlElement("Storage")]
        public List<StorageItem> StorageItems { get; set; }

        public Storage()
        {
            LastId = 0;
            StorageItems = new List<StorageItem>();
        }

        public static Storage Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
