﻿using MiningLog.DB;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogForMining.DB.Result
{
    public class AllResults : XmlSerializable<AllResults>
    {
        const string FILENAME = "result.xml";
        //this is to autoincrement the Id from Result
        [XmlAttribute("LastId")]
        public long LastId { get; set; }

        [XmlElement("Results")]
        public List<Result> Results { get; set; }

        public AllResults()
        {
            LastId = 0;
            Results = new List<Result>();
        }

        public static AllResults Load()
        {
            return ReadFromXML(FILENAME);
        }

        public void Save()
        {
            SaveToXML(FILENAME);
        }
    }
}
