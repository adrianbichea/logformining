﻿using System;
using System.Xml.Serialization;

namespace LogForMining.DB.Result
{
    //Fields:
    //  Id      autoincrement
    //  Date    datetime

    public class Result
    {
        [XmlAttribute("Id")]
        public long Id { get; set; }

        [XmlAttribute("Date")]
        public DateTime Date { get; set; }
    }
}
