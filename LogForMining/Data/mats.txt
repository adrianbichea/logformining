		Adomasite Stone	Ore	0.6	 	Adomasite Ingot, Galvesperdite Ingot	3,3,3,3	Mined	Planet Arkadia, Planet Calypso
		Alferix Stone	Ore	0.95	 	Alferix Ingot	3,3,3,3,3	Looted, Mined	Next Island, Planet Arkadia, Planet Calypso, Planet Cyrene, ROCKtropia
		Alternative Rock	Ore	0.01	138.00%	Alternative Ingot	3	Mined	ROCKtropia
		Aqeeq Stone	Ore	0.02	 	Aqeeq Ingot	3	Mined	Planet Toulan
		Arekite Stone	Ore	0.1	 	Arekite Ingot	3	Mined	Planet Arkadia
		Azzurdite Stone	Ore	1.2	104.50%	Azzurdite Ingot, Lystionite Steel Ingot	3,1,1,3	Looted, Mined	Planet Arkadia, Planet Calypso
		Banite Stones	Ore	0.08	 	Banite Ingot	3	Mined	Planet Arkadia
		Belkar Stone	Ore	0.02	105.00%	Belkar Ingot	3,3,3,3	Looted, Mined	Planet Arkadia, Planet Calypso, Planet Toulan, ROCKtropia
		Blausariam Stone	Ore	0.04	104.63%	Blausariam Ingot, Galaurphite Ingot	2,2,3,2,3,3	Looted, Mined	Planet Arkadia, Planet Calypso, ROCKtropia
		Blues Rock	Ore	0.02	 	Blues Ingot	3	Mined, Looted	ROCKtropia
		Caldorite stone	Ore	0.17	 	Caldorite Ingot	3,3,3,3	Looted, Mined	Hell, Planet Arkadia, Planet Calypso, Planet Toulan
		Cobalt Stone	Ore	0.2	106.70%	Cobalt Ingot, Cobaltium Ingot	4,3,4,3	Looted, Mined	Planet Arkadia, Planet Calypso
		Copper Stone	Ore	0.16	 	Copper Ingot	3,3,3	Looted, Mined	Planet Arkadia, Planet Calypso, ROCKtropia
		Cordul Stone	Ore	0.2	 	Cordul Ingot	3	Mined	Planet Arkadia
		Cumbriz Stone	Ore	0.15	101.50%	Cumbriz Ingot	3	Looted, Mined	Entropia Universe
		Cyrenium Ore	Ore	0.5	 	Cyrenium Ingot	3	Mined	Planet Cyrene
		Dark Lysterium	Ore	0.02	 	Dark Lysterium Bar	3	Mined	Next Island
		Dianum Ore	Ore	1.25	107.00%	Chalinum Alloy, Dianum Ingot	1,3	Looted, Mined	Entropia Universe
		Durulium Stone	Ore	0.8	109.00%	Cobaltium Ingot, Durulium Ingot	1,3	Looted, Mined	Entropia Universe
		Erdorium Stone	Ore	0.4	 	Erdorium Ingot	3	Looted, Mined	Entropia Universe
		Erionite Stone	Ore	0.2	 	Erionite Ingot	3	Mined	Entropia Universe
		Fairuz Stone	Ore	0.05	 	Fairuz Ingot	3	Mined	 
		Folk Rock	Ore	0.03	 	Folk Ingot	3	Mined, Looted	ROCKtropia
		Frakite Stone	Ore	0.3	 	Frakite Ingot	3	Mined	Planet Arkadia
		Frigulite Stone	Ore	0.12	 	Frigulerian Dust, Frigulite Ingot	8,3	Looted, Mined	Entropia Universe
		Ganganite Stone	Ore	0.12	 	Ganganite Ingot	3	Mined	Entropia Universe
		Gazzurdite Stone	Ore	0.25	 	Gazzurdite Ingot, Solares Ingot	3,3	Looted, Mined	Entropia Universe
		Glam Rock	Ore	0.04	 	Glam Ingot	3	Looted, Mined	ROCKtropia
		Gold Stone	Ore	1	150.00%	Aurilinin Gel, Gold Ingot	3,1	Looted, Mined	Entropia Universe
		Grunge Rock	Ore	0.04	 	Grunge Ingot	3	Mined, Looted	ROCKtropia
		Hansidian Rock	Ore	0.01	 	Hansidian Ingot	3	Mined	Entropia Universe
		Hard Rock	Ore	0.05	 	Hard Ingot	3,3	Mined, Looted	Hell, ROCKtropia
		Hebredite Stones	Ore	0.25	 	Hebredite Ingot	3	Mined	Planet Arkadia
		Himi Rock	Ore	0.142	 	Himi Ingot	3	Mined	Entropia Universe
		Ignisium Stone	Ore	0.7	 	Ignisium Ingot	3	Looted, Mined	Entropia Universe
		Iolite Stone	Ore	0.2	 	Iolite Ingot	3	Mined	Entropia Universe
		Iridium Ore	Ore	0.05	 	Iridium Ingot	3	Mined	Planet Cyrene
		Iron Stone	Ore	0.13	106.00%	Iron Ingot, Lystionite Steel Ingot	3,3	Looted, Mined	Entropia Universe
		Jazz Rock	Ore	0.06	 	Jazz Ingot	3	Mined	ROCKtropia
		Kaisenite Stone	Ore	0.02	 	Kaisenite Ingot	3	Mined, Looted	Planet Cyrene
		Kanerium Ore	Ore	2.5	135.00%	Kanerium Ingot	3	Looted, Mined	Planet Calypso
		Kaz Stones	Ore	0.04	 	Kaz Ingot	3	Mined, Looted	Planet Arkadia
		Kirtz Stone	Ore	5.6	 	Kirtz Ingot	3	Mined	Entropia Universe
		Langotz Ore	Ore	0.9	 	Langotz Ingot	3	Looted, Mined	Entropia Universe
		Lanorium Stone	Ore	0.22	 	Lanorium Ingot	3	Mined	Entropia Universe
		Lidacon Stones	Ore	1.5	 	Lidacon Ingot	3	Mined	Planet Arkadia
		Lulu Stone	Ore	0.12	 	Lulu pearl	3	Mined	Planet Toulan
		Lysterium Stone	Ore	0.01	103.00%	Lysterium Ingot, Lystionite Steel Ingot	22,3	Looted, Mined	Entropia Universe
		Maganite Ore	Ore	1.05	 	Maganite Ingot	3	Looted, Mined	Entropia Universe
	 	Maladrite Stone	Ore	0.04	 	Maladrite Ingot	3	Mined	Monria
		Maro Stone	Ore	0.13	 	Maro Ingot	4	Mined	Planet Toulan
		Megan Stone	Ore	0.18	120.00%	Megan Ingot	3	Looted, Mined	Entropia Universe
		Melatinum Stone	Ore	0.1	 	Melatinum Ingot	3	Mined	Planet Arkadia
		Morpheus Stone	Ore	0.83	 	Morpheus Ingot	3	Mined	Entropia Universe
	 	Motorhead Keg	Ore	0.01	150.00%	Ruxxnet Code	10	Found	ROCKtropia
		Narcanisum Stone	Ore	0.08	 	Narcanisum Ingot	3	Looted, Mined	Entropia Universe
		Niksarium Stone	Ore	0.65	107.00%	Niksarium Ingot	3	Looted, Mined	Entropia Universe
		Nosifer Stone	Ore	0.08	 	Nosi Ingot	3	Mined	Planet Cyrene
		Olerin Stone	Ore	0.07	 	Olerin Ingot	3	Mined	Planet Cyrene
		Ospra Stones	Ore	0.02	 	Ospra Ingot	3	Mined, Looted	Planet Arkadia
		Petonium Stone	Ore	1.79	 	Magtonium Dust, Petonium Ingot	3	Looted, Mined	F.O.M.A. - Fortuna
		Platinum Stone	Ore	3	111.00%	Platinum Ingot	3	Looted, Mined	Entropia Universe
		Praetonium	Ore	0.02	 	Praetonium Ingot	2	Looted, Mined, Missi	Next Island
		Punk Rock	Ore	0.07	 	Punk Ingot	3	Mined, Looted	ROCKtropia
		Pyrite Stone	Ore	0.2	 	Pyrite Ingot	3	Looted, Mined	Entropia Universe
		Qasdeer Stone	Ore	0.25	 	Qasdeer Ingot	3	Mined	Planet Toulan
		Quantium Stone	Ore	0.6	133.00%	Quantium Ingot	3	Looted, Mined	Entropia Universe
		Redulite Ore	Ore	2.2	 	Redulite Ingot	3	Mined	Entropia Universe
		Reggea Rock	Ore	0.08	115.00%	Reggea Ingot	3	Mined, Looted	ROCKtropia
		Rugaritz Ore	Ore	1.5	 	Rugaritz Ingot	3	Looted, Mined	Entropia Universe
	 	Sorensens Stone	Ore	0.03	 	Refined Sorensens Stone	3	Mined	Next Island
		Sothorite Ore	Ore	0.09	 	Sothorite Ingot	3	Mined	Planet Cyrene
		Sunburst Stone	Ore	0.11	 	Sunburst Ingot	7	 	Planet Toulan
		Tananite Ore	Ore	0.15	 	Leethiz Ingot	3	Mined	Planet Cyrene
		Techno Rock	Ore	0.1	115.00%	Techno Ingot	3	Mined, Looted	ROCKtropia
		Telfium Stones	Ore	0.14	 	Telfium Ingot	3	Mined	Planet Arkadia
		Terrudite Stone	Ore	1.1	 	Terrudite Ingot	3	Looted, Mined	Entropia Universe
		Tridenite Ore	Ore	2	1350.00%	Tridenite Ingot	3	Looted, Mined	Entropia Universe
		Valurite Stone	Ore	6	 	Valurite Ingot	3	Looted, Mined	Entropia Universe
		Veda Stones	Ore	0.06	 	Veda Ingot	3	Mined, Looted	Planet Arkadia
		Vesperdite Ore	Ore	1.8	 	Galvesperdite Ingot, Vesperdite Ingot	3,3	Mined	Entropia Universe
		Wenrex Stones	Ore	0.17	 	Wenrex Ingot	3	Mined	Planet Arkadia
		Wiles Stone	Ore	0.3	 	Wiley Ingot	3	Mined	Planet Cyrene
		Xeremite Ore	Ore	4	190.00%	Xeremite Ingot	3	Looted, Mined	Entropia Universe
		Yashib Stone	Ore	0.13	 	Yashib Ingot	5	Mined	Planet Toulan
	 	Ycyan Ore	Ore	0.35	 	Ycy Ingot	3	Mined	Planet Cyrene
		Yulerium Stones	Ore	0.5	 	Yulerium Ingot	3	Mined	Planet Arkadia
		Zanderium Ore	Ore	2.5	 	Zanderium Ingot	3	Mined	Entropia Universe
		Zinc Stone	Ore	0.1	 	Galvesperdite Ingot, Zinc Ingot, Zincalicid Energy Crystal	3,3,7	Looted, Mined	Entropia Universe
		Zircon Stone	Ore	0.05	 	Zircon Ingot	3	Mined	Planet Toulan
		Zorn Star Ore	Ore	0.01	 	Zorn Star Ingot	3	Looted, Mined	Planet Cyrene
		Zulax Stones	Ore	0.65	 	Zulax Ingot	3	Mined	Planet Arkadia
		Acid Root	Enmatter	0.32	0.00%	Frigulerian Dust, Root Acid, Zincalicid Energy Crystal	2,4,3,4,2,4,2,3,3	Mined	Planet Arkadia, Planet Calypso, Planet Toulan
		Alicenies Liquid	Enmatter	0.05	110.00%	Alicenies Gel	2,2,2	Looted, Mined	Hell, Planet Arkadia, Planet Calypso
		Angelic Grit	Enmatter	0.5	110.11%	Angelic Flakes	2	Looted, Mined	Planet Calypso
		Angel Scales	Enmatter	0.01	1800.00%	Light Mail	2	Mined	Planet Calypso
		Ares Head	Enmatter	0.26	108.73%	Ares Powder, Solares Ingot	2,4,2,4	Mined	Planet Arkadia, Planet Calypso
		Azur Pearls	Enmatter	0.96	136.81%	Pearl Sand	2,2,2	Mined	Hell, Planet Arkadia, Planet Calypso
		Binary Fluid	Enmatter	0.75	120.00%	Binary Energy	2,2	Looted, Mined	Planet Arkadia, Planet Calypso
		Black Russian Cocktail Mix	Enmatter	0.03	 	Black Russian Cocktail	2,2	Mined, Looted	Hell, ROCKtropia
		Blood Moss	Enmatter	0.09	 	Medical Compress	2	Mined	Planet Calypso
		Blue Crystal	Enmatter	0.02	106.50%	Hardening Agent	2	Mined, Looted	Planet Cyrene
	 	Blue Ice Crystal	Enmatter	0.35	 	Chilling Agent	2	 	Planet Cyrene
		Bodai Dust	Enmatter	0.2	 	Bodai Filler	2	Mined	Planet Arkadia
		Cave Sap	Enmatter	0.39	 	Putty	2,2,2	Mined	Planet Arkadia, Planet Calypso, Planet Toulan
		Clear Crystal	Enmatter	0.1	118.00%	Lightening Agent	2	Mined	Planet Cyrene
		Crude Oil	Enmatter	0.01	105.00%	Chalinum Alloy, Oil	39,2	Looted, Mined, Found	Entropia Universe
		Devil's Tail	Enmatter	0.47	 	Chalmon	2,2	Looted, Mined	Entropia Universe, Planet Arkadia
		Dianthus Liquid	Enmatter	0.3	 	Dianthus Crystal Powder	2	Mined	Entropia Universe
		Dunkel Particle	Enmatter	0.55	5000.00%	Dunkel Plastix	2	Looted, Mined	Entropia Universe
		Edres Resin	Enmatter	0.13	 	Edres Varnish	2	Mined, Looted	Planet Arkadia
		Energized Crystal	Enmatter	0.3	 	Energized Crystal Cell, Zincalicid Energy Crystal	2,3	Mined	Entropia Universe
		Ferrum Nuts	Enmatter	1	 	Antimagnetic Oil, Zincalicid Energy Crystal	1,2	Mined	Entropia Universe
		Fire Root Globule	Enmatter	0.3	 	Fire Root Pellet	2	Mined	Entropia Universe
		Florican Mist	Enmatter	0.27	 	Florican Spray	2	Mined	Planet Arkadia
		Force Nexus	Enmatter	0.01	 	Distilled Sweat Crystal, Light Mind Essence, Mind Essence	1/100,1/100,1/200,3,3,1/200	Mined	Entropia Universe, ROCKtropia
		Garcen Grease	Enmatter	0.1	 	Garcen Lubricant	2	Looted, Mined	Entropia Universe
		Ghali	Enmatter	0.16	 	Ghali Powder	2	Mined	Planet Toulan
		Gorbek Emulsion	Enmatter	0.4	 	Gorbek Tallow	2	Mined	Planet Arkadia
		Green Crystal	Enmatter	0.03	106.00%	Life Essence	2	Mined	Planet Cyrene
		Growth Molecules	Enmatter	0.47	101.00%	Energized Fertilizer	1	Looted, Mined	Entropia Universe
		Harvey Wallbanger Cocktail Mix	Enmatter	0.04	 	Harvey Wallbanger Cocktail	2,2	Mined	Hell, ROCKtropia
		Hazy Crystal	Enmatter	0.3	 	Reflecting Agent	2	Mined	Planet Cyrene
		Henren Stems	Enmatter	0.63	 	Galvesperdite Ingot, Henren Cube	2,2	Mined	Entropia Universe
		Hurricane Cocktail Mix	Enmatter	0.01	 	Hurricane Cocktail	2,2	Mined	Hell, ROCKtropia
		Hydrogen Steam	Enmatter	0.02	 	Hydrogen Gas	2	Mined, Looted	Planet Arkadia
		Kamikaze Cocktail Mix	Enmatter	0.085	 	Kamikaze Cocktail	2	Mined, Looted	ROCKtropia
		Long Island Ice Tea Cocktail Mix	Enmatter	0.05	 	Long Island Ice Tea Cocktail	1,1	Mined, Looted	Hell, ROCKtropia
		Lotium Fluid	Enmatter	0.3	 	Lotium Gel	2	Mined	Planet Arkadia
		Lumis Leach	Enmatter	0.42	 	Aurilinin Gel, Light Liquid	2,2	Mined	Entropia Universe
		Lytairian Dust	Enmatter	0.19	104.00%	Lytairian Powder	2	Looted, Mined	Entropia Universe
		Magerian Mist	Enmatter	0.25	 	Frigulerian Dust, Magerian Spray, Magtonium Dust	3,2	Looted, Mined	Entropia Universe
		Mai Tai Cocktail Mix	Enmatter	0.015	 	Mai Tai Cocktail	2,2	Mined	Hell, ROCKtropia
		Mamnoon	Enmatter	0.08	 	Mamnoon Mist	2	Mined	Planet Toulan
		Melchi Water	Enmatter	0.02	104.00%	Chalinum Alloy, Distilled Sweat Crystal, Melchi Crystal	2,18,8	Looted, Mined	Entropia Universe
		Mojito Cocktail Mix	Enmatter	0.02	 	Mojito Cocktail	2,2	Mined	Hell, ROCKtropia
		Nawa Drops	Enmatter	0.01	 	Nawa Vial	2	Mined	Planet Toulan
		Nawa Fragment	Enmatter	0.01	 	Nawa Shard	2	Rig	Planet Toulan
		Nirvana Cocktail Mix	Enmatter	0.02	 	Nirvana Cocktail	2,2	Mined	Hell, ROCKtropia
		Orange Crystal	Enmatter	0.07	152.00%	Softening Agent	2	Mined	Planet Cyrene
		Pel Crystals	Enmatter	0.5	 	Pel Liquid	2	Mined	Planet Arkadia
	 	Phlogiston	Enmatter	0.04	 	Concentrated Phlogiston	3	Mined	Next Island
		Pina Colada Cocktail Mix	Enmatter	0.025	 	Pina Colada Cocktail	2,2	Mined	Hell, ROCKtropia
		Purple Crystal	Enmatter	0.08	131.00%	Darkening Agent	2	Mined	Planet Cyrene
	 	Qaz Worm	Enmatter	0.06	 	Hareer Thread	4	Mined	Planet Toulan
		Quil Sap	Enmatter	0.25	 	Quil Rubber	2	Mined	Planet Arkadia
		Rainbow Crystal	Enmatter	0.15	420.00%	Color Enhancing Agent	2	Mined	Planet Cyrene
		Red Molten Crystal	Enmatter	0.5	 	Heating Agent	2	 	Planet Cyrene
		Sham	Enmatter	0.05	 	Sham Sand	2	Mined	Planet Toulan
		Solis Beans	Enmatter	0.78	140.00%	Solares Ingot, Solis Paste	2,2	Looted, Mined	Entropia Universe
		Somin Tar	Enmatter	0.06	 	Somin Glue	2	Mined, Looted	Planet Arkadia
		Star Particles	Enmatter	0.08	 	Star Dust	2	Mined	Planet Arkadia
		Sweetstuff	Enmatter	0.01	 	Nutrio Bar	1	Mined	Entropia Universe
		Typonolic Steam	Enmatter	0.15	128.00%	Frigulerian Dust, Typonolic Gas	3,2	Looted, Mined	Entropia Universe
		Vegatation Spores	Enmatter	0.4	 	Inhaler	2	Mined	Entropia Universe
		Vorn Pellets	Enmatter	0.2	 	Vorn Plastic	2	Mined	Planet Arkadia
		Whiskey Sour Cocktail Mix	Enmatter	0.075	 	Whiskey Sour Cocktail	2,2	Mined, Looted	Hell, ROCKtropia
		Xelo Haze	Enmatter	0.39	 	Xelo Vapour	2	Mined	Planet Arkadia
		Yellow Crystal	Enmatter	0.01	111.00%	Super Adhesive	2	Mined	Planet Cyrene
		Youd	Enmatter	0.04	 	Youd Bottle	2	Mined	Planet Toulan
		Zoldenite Dust	Enmatter	0.04	 	 	 	Mined	Monria
		Zolphic Oil	Enmatter	0.04	 	Zolphic Grease	2	Mined, Looted	Planet Arkadia
		Aakas Alloy	Treasure	0.1	 	Aakas Plating	3	Mined	Planet Arkadia
		Aarkan Pellets	Treasure	0.02	105.00%	Aarkan Polymer	3	Mined, Looted	Planet Arkadia
		Alkar Crystals	Treasure	0.05	 	Alkar Lattice	3	Mined	Planet Arkadia
		Arkadian Golden Key Barrel	Treasure	18	179.00%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Bearings	Treasure	2	133.64%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Codex Chamber	Treasure	40	120.00%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Cog	Treasure	10	158.50%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Gears	Treasure	1	200.00%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Housing	Treasure	30	166.67%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Inner Ring	Treasure	20	105.00%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Interface	Treasure	12	104.00%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Outer Ring	Treasure	30	206.67%	 	 	Looted	Planet Arkadia
		Arkadian Golden Key Power Source	Treasure	6	111.33%	 	 	Looted	Planet Arkadia
		Bismuth Fragment	Treasure	0.2	550.00%	Bismuth Plating	3	Mined	Planet Arkadia
		ChemSet	Treasure	0.3	 	Binding Epoxy	3	Mined	Planet Arkadia
		Diagen Firedrops	Treasure	0.04	 	Diagen Matrix	3	Mined	Planet Arkadia
		Hexra Gems	Treasure	0.1	 	Hexra Electroplate	3	Mined	Planet Arkadia
		Khorudoul Extrusion	Treasure	0.08	 	Khorudoul Polymer	3	Mined	Planet Arkadia
		Khorum Alloy	Treasure	0.2	 	Khorum Plating	3	Mined	Planet Arkadia
		Part Of Brass Alloy	Treasure	0.12	 	Brass Alloy	3	Mined	Planet Arkadia
		Part Of Bronze Alloy	Treasure	0.1	 	Bronze Alloy	3	Mined	Planet Arkadia
		Part Of Flint Arrow Head	Treasure	0.08	 	Flint Arrow Head	3	Mined	Planet Arkadia
		Part Of Flint Axe Head	Treasure	0.06	 	Flint Axe Head	3	Mined	Planet Arkadia
		Part Of Fly Amber	Treasure	0.1	 	Fly Amber	3	Mined	Planet Arkadia
		Part Of Fossil Ammonite	Treasure	0.02	 	Fossil Ammonite	3	Mined	Planet Arkadia
		Part Of Fossil Tooth	Treasure	0.01	 	Fossil Tooth	3	Mined	Planet Arkadia
		Part Of Mosquito Amber	Treasure	0.3	 	Mosquito Amber	3	Mined	Planet Arkadia
		Pirrel Pellets	Treasure	0.3	 	Pirrel Hexaton	3	Mined	Planet Arkadia
		Quenta Flux	Treasure	0.15	 	Quenta Panel	3	Mined, Looted	Planet Arkadia
	 	Regula Lode	Treasure	0.2	 	Regula Powder	3	Mined	Planet Arkadia
		Songkra Alloy	Treasure	0.25	 	Songkra Plating	3	Mined	Planet Arkadia
		Songtil Agent	Treasure	0.06	 	Balancing Agent	3	Mined	Planet Arkadia
		Starrix Nodes	Treasure	0.25	 	Starrix Quilting	3	Mined	Planet Arkadia
		Vedacore Fibre	Treasure	0.15	 	Vedacore Sheeting	3	Mined	Planet Arkadia